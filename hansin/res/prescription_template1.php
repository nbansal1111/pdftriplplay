<style type="text/css">
body
{
    margin:0;
    /*font-family: 'Roboto-regular', sans-serif;*/
}
h1, h2, h3, h4, h5, h6, p
{
    margin-top: 0;
    margin-bottom: 0;
    margin-left: 0;
    margin-right: 0;
}

.midwrap
{
    float: left;
    width: 100%;
}
.header
{
    float: left;
    width: 100%;
    margin-top: 25px;
}
.headertop
{
    float: left;
    width: 100%;
    margin-bottom: 10px;
}
.header .logo
{
    float: left;
}
.header .logo img
{
    float: left;
}
.header .headerRight
{
    float: right;
}
.headerRight h3
{
    float: left;
    width: 100%;
    text-align: right;
    font-size: 14px;
    color: #c1493c;
}
.headerRight p
{
    float: left;
    width: 100%;
    text-align: right;
    font-size: 11px;
    color: #333;
}
.headerBottom
{
    float: left;
    background-color: #e2bebe;
    padding-left: 10px;   
    padding-right: 10px;   
    padding-top: 10px;   
    padding-bottom: 10px;   
 }
.headerBottom .leftbottom
{
    float: left;
     color: #333;
    font-size: 12px;
}
.headerBottom .rightbottom
{
    float: right;
     color: #333;
    font-size: 12px;
    color: #c1493c;
    text-align: right;
}

.contentBody
{
    float: left;
    width: 100%;
    min-height:300px;
}


.footer
{
    float: left;
    background-color: #c1493c;
    padding: 15px 5%;
    color: #fff;
    font-size: 12px;
    text-align: center;
}
.default_wrap
{
    float: left;
    width: 650px;
    margin-top:10px;
    margin-bottom:10px;
    padding-left:15px;
    padding-right:15px;
}
.patientDetails
{
    float:left;
    padding-top:10px;
    padding-bottom:10px;
    background-color: #f7f7f7;
}
.default_wrap .patientLeft
{
    float: left;
}
.default_wrap .patientRight
{
    float: right;
    text-align: right;
}
.patientLeft h3, .patientRight h3
{
    /*font-family: 'Roboto-Bold', sans-serif;*/
    font-size: 12px;
    color: #222222;
}
.patientLeft p, .patientRight p
{
    font-size: 11px;
    color: #6b6b6b;
}


hr
{
    float: left;
    width: 100%;
    border-bottom: 1px #333 solid;
}

.prescriptionTable
{
    font-size: 12px;
    width: 695px;
    border-color: #ccc;
    border-collapse: collapse;
}
.prescriptionTable thead
{
    background-color: #ffd8aa;
}

tr
{
    float: left;
}
.prescriptionTable thead tr th
{
    border:1px #f1ba79 solid;
    background-color: #ffd8aa;
    font-size: 12px;
}
.prescriptionTable tbody
{
    background-color: #f7f7f7;
}
.prescriptionTable tbody tr td
{
    border:1px #ccc solid;
    font-size: 11px;
    color: #666;
}
</style>
<page backtop="45mm" backbottom="20mm" style="font-family: freeserif; page-break-inside:avoid; height: 100%;">
    <page_header>
        <div class="header">
            <div class="headertop">
                <table style="width: 340px;">
                <tr>
                <td>
                <div class="logo">
                    <img src="examples/res/logo.png">
                </div>
                </td>
                <td>
                <div class="headerRight">
                    <h3>Dr. Mohan K.T.</h3>
                    <p>M.D. (Critical Care Medicare USA)<br>
                    M.D. (Medicine, Comit / NYH University USA)<br>
                    FACP (Fellow of American College of Physician USA)<br>
                    ABIM (American Board Certified in Medicine)<br>
                    Physician, Diabetologist, Heart & Thyroid Specialist<br>
                    R.g. No: 2011/04/779
                    </p>

                </div>
                </td>
                </tr>
                </table>
            </div>

            <div class="headerBottom">
                <table style="width: 325px;">
                <tr>
                <td>
                <div class="leftbottom"><b>Consulting Hours :</b> 9am to 12pm (Mon to Sat)</div>
                </td>
                <td>
                <div class="rightbottom"><b>For Appointment Call : 8888 568 777, 5888 746 777</b></div>
                </td>
                </tr>
                </table>
            </div>
        </div>
    </page_header>
    <page_footer>
        <div class="footer">
            Aundh-Himewadi Road, Jagtap Dairy, Vishal Nagar, Pune - 411027, Email: response.applehospitle@gmail.com
        </div>
    </page_footer>


    <div class="midwrap">   

        <div class="contentBody">
            <div class="patientDetails default_wrap">
                
                <table style="width: 325px;">
                <tr>
                <td>
                <div class="patientLeft">
                    <h3>Patient Details</h3>
                    <p>Swati Rastogi, F/35 yrs<br>
                    UPID/123456</p>
                </div>
                </td>

                <td>
                <div class="patientRight">
                    <h3>25th March 2017, 18:00 Hr</h3>
                    <p>Height: 153 cms<br>
                    Weight: 58 Kgs</p>
                </div>
                </td>
                </tr>
                </table>

            </div>

            <div class="default_wrap">
                <div class="patientLeft">
                    <h3>System Review</h3>
                    <p>This comprise the systemn review as captured by the doctor.</p>
                </div>
            </div>

            <div class="default_wrap">
                <div class="patientLeft">
                    <h3>Vitals</h3>
                    <p>B.P : 120/90 ; SPo2: 94%</p>
                </div>
            </div>

            <div class="default_wrap">
                <div class="patientLeft">
                    <h3>Diagnosis</h3>
                    <p>Disease/illness name</p>
                </div>
            </div>

            <div class="default_wrap">
                <div class="patientLeft">
                    <h3>Further Investigation</h3>
                    <p>Tests/ Scans</p>
                </div>
            </div>

            <div class="default_wrap"><hr></div>

            
                <table class="prescriptionTable">
                    <col style="width: 20%" class="col1">
                    <col style="width: 7%">
                    <col style="width: 7%">
                    <col style="width: 7%">
                    <col style="width: 7%">
                    <col style="width: 7%">
                    <col style="width: 7%">
                    <col style="width: 7%">
                    <col style="width: 7%">
                    <col style="width: 7%">
                    <col style="width: 15%">
                    <thead>
                        <tr>
                            <th rowspan="2">Name of medicine<br><small>औषधे</small></th>
                            <th colspan="2">Breakfast<br>नाश्ता</th>
                            <th colspan="2">Lunch<br>दुपारचे जेवण</th>
                            <th colspan="2">Evening Snack<br>संध्याकाळचा नाश्ता</th>
                            <th colspan="2">Dinner<br>रात्रीचे जेवण</th>
                            <th rowspan="2">Days<br><small>दिवस     </small></th>
                            <th rowspan="2">Comments</th>
                        </tr>
                        <tr>
                            <th style="color:#8f7900;">Before<br>पूर्वी</th>
                            <th style="color:#8f7900;">After<br>नंतर</th>
                            <th style="color:#8f7900;">Before<br>पूर्वी</th>
                            <th style="color:#8f7900;">After<br>नंतर</th>
                            <th style="color:#8f7900;">Before<br>पूर्वी</th>
                            <th style="color:#8f7900;">After<br>नंतर</th>
                            <th style="color:#8f7900;">Before<br>पूर्वी</th>
                            <th style="color:#8f7900;">After<br>नंतर</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php for($i=0; $i<60; $i++){?>
                        <tr align="center">
                            <td>Sompaz D 40</td>
                            <td> </td>
                            <td> </td>
                            <td> </td>
                            <td> </td>
                            <td> </td>
                            <td> </td>
                            <td> </td>
                            <td> </td>
                            <td> </td>
                            <td>1 cap</td>
                        </tr>
                        <?php }?>
                        


                    </tbody>
                </table>
            

            <div class="default_wrap">
                <div class="patientLeft">
                    <h3>Patient's Notes</h3>
                    <p>Lorem ipsum dolor site amet, consecteture adipiscing elit. Aenean commodo ligula eget dolor.</p>
                </div>
            </div>

            <div class="default_wrap">
                <div class="patientLeft">
                    <h3>Follow Up</h3>
                    <p>2nd April 2017</p>
                </div>
            </div>
        </div>
    </div>
</page>