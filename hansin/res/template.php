<!DOCTYPE html>
<html>
<head>
    <title></title>
</head>
<body>

<style type="text/css">

body {
    margin:0;
    line-height: 130%;
    font-family: Arial, sans-serif;
    font-size: 12px;
}
*{box-sizing: border-box;}
a{text-decoration: none;color: #3D4DAC;}
ol, ul {
    list-style: none;
    margin:0;
    padding:0;
}
blockquote, q {
    quotes: none;
}
blockquote:before, blockquote:after,
q:before, q:after {
    content: '';
    content: none;
}
table {
    border-collapse: collapse;
    border-spacing: 0;
}
table, tbody, tr, td
        {
            float: left;
        }
b{font-weight: bold;}
h5
        {
            font-size: 14px;
            float: left;
            margin: 3px 0;
        }
h4
        {
            font-size: 16px;
            float: left;
        }
h3
        {
            font-size: 18px;
            float: left;
        }
h2
        {
            font-size: 20px;
            float: left;
        }
h1
        {
            font-size: 22px;
            float: left;
        }
.greyButton
        {
            float: left;
            color: #fff;
            padding: 3px 6px 1px 6px;
            background-color: #333;
            margin: 0 5px;
            line-height: 12px;
            font-size: 10px;
        }
.fullWrap
        {
            float: left;
            width: 650px;
        }
.pullRight
        {
            float: right;
        }
.halfWrap
        {
            float: left;
            width: 50%;
        } 
.center
        {
            text-align: center;
        } 
.padding10
        {
            padding:10px;
        } 
.padding2
{
    padding:2px;
} 
.headerLeftWrap
        {
            float: left;
            width: 300px;
        }
.headerLeftWrap p
        {
            font-size: 10px;
        }
.midWrap
        {
            width: 640px;
            float:left;
        }
.logoWrap
        {
            margin: auto;
            width: 80px;
            height: 30px;
            background-color: #ccc;
        }
.headerLeftWrap h5, .headerLeftWrap p
        {
            float: left;
            width: 100%;
        }
.srfWrap
        {
            float: left;
            width: 100%;
            border: 1px solid #000; 
            margin-top: 5px;
        }
.srfWrap span
        {
            float: left;
            padding: 5px 15px;
            background-color: #000;
            color: #fff;
            line-height: 9px;
        }
.headerRightWrap
        {
            float: right;
            width: 100px;
        }
.passportPhoto
        {
            float: left;
            width: 80px;
            height: 100px;
            margin: 0 10px;
            border: 1px solid #000;
        }
.passportTopText
        {
            float: left;
            width: 100%;
            text-align: center;
        }
.rowBorder
        {
            border: 1px solid #000;
            border-bottom: none;
        }
.borderBottom
        {
            border-bottom: 1px solid #000;
        }    
.formWrap tr:last-child
        {
            border-bottom: 1px solid #000;
        }    
.fillBox
        {
            float: left;
            width: 100%;
        }
.fillBox li 
        {
            float: left;
            width: 15px;
            text-align: center;
            height: 16px;
            border-left: 1px solid #000;
            padding: 3px 0;
        } 

.nameCol
        {
            float: left;
            width: 170px;
            line-height: 12px;
        }
.rightColWidth
        {
            width:calc(100% - 170px);
        }
.headBgColor
        {
            background-color: #ccc;
        }
.signWrap
        {
            float: left;
            width: 200px;
            height: 50px;
            border: 1px solid #000;
        }
.bottomList
        {
            float: right;
            margin-top: 35px;
        }
.bottomList li
        {
            float: left;
            margin-left: 5px;
            font-size: 10px;
        }
.addressBox li:nth-child(-n+56)
        {
            border-bottom:1px solid #000;
        }
        
        
</style>
    <?php
        // calling API
        // function CallAPI($method, $url, $data = false){
        //     $curl = curl_init();
        //     switch ($method)
        //     {
        //         case "POST":
        //             curl_setopt($curl, CURLOPT_POST, 1);
        //             if ($data)
        //                 curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        //             break;
        //         case "PUT":
        //             curl_setopt($curl, CURLOPT_PUT, 1);
        //             break;
        //         default:
        //             if ($data)
        //                 $url = sprintf("%s?%s", $url, http_build_query($data));
        //     }
        //     // Authentication:
        //     curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        //     // curl_setopt($curl, CURLOPT_USERPWD, "username:password");
        //     curl_setopt($curl, CURLOPT_URL, $url);
        //     curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        //     curl_setopt($curl, CURLOPT_HTTPHEADER, array(
        //         'Content-Type: application/json',
        //         'roleType: 4',
        //         'authToken: 12345678'
        //         ));
        //     $result = curl_exec($curl);
        //     curl_close($curl);
        //     return $result;
        // }


        // parsing data
        // $jsonFile = file_get_contents('http://localhost/hansin/data.json');
        // // $json = json_decode($jsonFile);
        // $result = CallAPI('GET', 'http://13.126.28.66:8080/syntagi/prescription/find/patient?patientId=590c04dfe4b043b200339259');
        // $json = json_decode($result, true);
        // print_r($json['data']);
        // $custName = $json['data'][0]['patientName'];
        // include_once '../api/api.php';
    // print_r($resJson);
        print_r($resJsonObj);
        $custName = $resJsonObj['name'];
        $fullAdd = $resJsonObj['billingAddress']['flatNum'] .' '. $resJsonObj['billingAddress']['locality'] .' '. $resJsonObj['billingAddress']['landmark'] .' '. $resJsonObj['billingAddress']['city'] .' '. $resJsonObj['billingAddress']['state'] .' '. $resJsonObj['billingAddress']['pinCode'];
        $phNum = $resJsonObj['phoneNumber'];
        $altNum = $resJsonObj['altPhoneNumber'];
        $email = $resJsonObj['email'];
        // $custName = $resJsonData->name;
        // $fullAdd = $resJsonData->billingAddress->flatNum .' '. $resJsonData->billingAddress->locality .' '. $resJsonData->billingAddress->landmark .' '. $resJsonData->billingAddress->city .' '. $resJsonData->billingAddress->state .' '. $resJsonData->billingAddress->pinCode;
        // $phNum = $resJsonData->phoneNumber;
        // $altNum = $resJsonData->altPhoneNumber;
        // $email = $resJsonData->email;
    ?>
<page backtop="5mm" backbottom="5mm" style="font-family: freeserif; page-break-inside:avoid; height: 100%;">

    <div class="midWrap">
            <table class="fullWrap">
                <tbody class="fullWrap">
                    <tr class="padding10 fullWrap">
                        <td class="headerLeftWrap">
                            <div class="logoWrap"></div>
                            <b class="fullWrap" style="margin-top:5px;">HANSIN IT SERVICES PVT. LTD</b>
                            <p>
                                6th Floor, Malik Plaza, Opp. S.D. College, G.T. Road,
                                <br> Panipat-132103 (Haryana) Tel.: +91 180 - 2631111,
                                <br> E-Mail:- sales@hansinit.com, www.hansinit.com
                            </p>
                            <div class="srfWrap">
                                <span>SRF / CRF No. :</span>
                            </div>
                        </td>
                        <td class="headerRightWrap">
                            <b class="passportTopText">(Class - C ISP)</b>
                            <div class="passportPhoto"></div>
                        </td>
                    </tr>
                </tbody>
            </table>
        <table class="fullWrap formWrap">
            <tbody class="fullWrap">
                <tr class="padding2 fullWrap">
                    <td class="fullWrap">
                        <h3 class="fullWrap center">
                            <b>Customer Application Form (CAF)</b>
                        </h3>
                    </td>
                </tr>
                <tr class="fullWrap rowBorder headBgColor">
                    <td class="padding2">
                        <b>General Information</b>
                    </td>
                </tr>
                <tr class="fullWrap rowBorder">
                    <td class="nameCol padding2">
                        <b>Subscriber Name</b>
                    </td>
                    <td>
                        <ul class="fillBox">

                        <?php 
                            for($i = 0; $i < 28; $i++ ){ 
                        ?>
                            <li>
                                <?php 
                                    if($i < strlen($custName)){
                                        echo $custName[$i];
                                    }
                                ?>
                            </li>
                        <?php } ?>
                        </ul>
                    </td>
                </tr>
                <tr class="fullWrap rowBorder">
                    <td class="nameCol padding2">
                        <b>Father's & Husband Name</b>
                    </td>
                    <td>
                        <ul class="fillBox">
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ul>
                    </td>
                </tr>
                <tr class="fullWrap rowBorder">
                    <td class="nameCol padding2">
                        <b>Subscriber Type (Pone)</b>
                    </td>
                    <td>
                        <div class="greyButton">Corporate/SME</div>
                        <div class="greyButton">Institutional</div>
                        <div class="greyButton">Individual/Home </div>
                        <div class="greyButton">Organization</div>
                    </td>
                </tr>
                <!-- <tr class="fullWrap rowBorder">
                    <td class="nameCol padding2">
                        <b>Local Address
                            <br>(For Billing & Termination)</b>
                    </td>
                    <td class="rightColWidth">
                        <ul class="fillBox addressBox">
                        <?php 
                            for($i = 0; $i < 84; $i++ ){ 
                        ?>
                            <li>
                                <?php
                                    if($i < strlen($fullAdd)){
                                        echo $fullAdd[$i];
                                    };
                                ?>
                            </li>
                        <?php } ?>
                        </ul>
                    </td>
                </tr>
                <tr class="fullWrap rowBorder">
                    <td class="nameCol padding2">
                        <b>Wired Line Phone No.</b>
                    </td>
                    <td>
                        <ul class="fillBox">
                        <?php 
                            for($i = 0; $i < 13; $i++ ){ 
                        ?>
                            <li>
                                <?php 
                                    if($i < strlen($altNum)){
                                        echo $altNum[$i];
                                    }
                                ?>
                            </li>
                        <?php } ?>
                        </ul>
                    </td>
                    <td class="nameCol padding2" style="width: 75px;">
                        <b>Mobile No.</b>
                    </td>
                    <td>
                        <ul class="fillBox">
                        <?php 
                            for($i = 0; $i < 10; $i++ ){ 
                        ?>
                            <li>
                                <?php 
                                    if($i < strlen($phNum)){
                                        echo $phNum[$i];
                                    }
                                ?>
                            </li>
                        <?php } ?>
                        </ul>
                    </td>
                </tr> -->
                <tr class="fullWrap rowBorder">
                    <td class="nameCol padding2">
                        <b>Address Type (Pone)</b>
                    </td>
                    <td>
                        <div class="greyButton">Owned</div>
                        <div class="greyButton">Rented</div>
                    </td>
                    <td style="float:right;">
                        <?php echo $email; ?>
                    </td>
                    <td class="nameCol padding2" style="float:right;width: 60px;border-right:1px solid #000;margin-right: 10px;">
                        <b>E-mail</b>
                    </td>
                </tr>
                
                <tr class="fullWrap rowBorder headBgColor">
                    <td class="padding2">
                        <b>Service Details </b>
                    </td>
                </tr>
                <tr class="fullWrap rowBorder">
                    <td class="nameCol padding2">
                        <b>Required Service (Pone)</b>
                    </td>
                    <td>
                        <div class="greyButton">Fixed Wi-Fi Broadband</div>
                        <div class="greyButton">Broadband on Cable</div>
                        <div class="greyButton">DIA-LL(Direct Internet Access )</div>
                    </td>
                </tr>
                <tr class="fullWrap rowBorder">
                    <td class="nameCol padding2">
                        <b>Service Delivery (Pone)</b>
                    </td>
                    <td>
                        <div class="greyButton">On Fixed Wi-Fi CPE</div>
                        <div class="greyButton">Copper / Ethernet Cable</div>
                        <div class="greyButton">Optical Fiber (Fiber to Home) </div>
                    </td>
                </tr>
                <tr class="fullWrap rowBorder">
                    <td class="nameCol padding2">
                        <b>Plan & Speed</b>
                    </td>
                    <td style="width: 150px;line-height:130%;">
                        As per plan sheet
                    </td>
                    <td class="nameCol padding2" style="width: 160px;border-right:1px solid #000;margin-right: 10px;">
                        <b>Required Static Public IP</b>
                    </td>
                    <td style="line-height:130%">
                        
                    </td>
                </tr>
                <tr class="fullWrap rowBorder">
                    <td class="nameCol padding2">
                        <b>Traffic Limit (UL/DL)</b>
                    </td>
                    <td>
                        <b> As per Fair Uses Policy & as per Tariff Plan</b>
                    </td>
                </tr>
                <tr class="fullWrap rowBorder headBgColor">
                    <td class="padding2">
                        <b>Financial Charges Details</b>
                    </td>
                </tr>
                <tr class="fullWrap rowBorder">
                    <td class="padding2" style="width: 50%">
                        <b>One Time Activation (Non refundable)</b>
                    </td>
                    <td class="padding2" style="width: 50%;border-left:1px solid #000">
                        <b> Security Deposit (Refundable)</b>
                    </td>
                </tr>
                <tr class="fullWrap rowBorder">
                    <td class="padding2" style="width: 50%">
                        <b>CPE Cost (Non refundable)</b>
                    </td>
                    <td class="padding2" style="width: 50%;border-left:1px solid #000">
                        <b>CPE Rental (Non refundable)</b>
                    </td>
                </tr>
                <tr class="fullWrap rowBorder">
                    <td class="" style="width: 50%">
                        <b class="nameCol padding2" style="float: left">Subscription Type (Pone)</b>
                        <div class="greyButton">Prepaid</div>
                        <div class="greyButton">Post Pay</div>
                    </td>
                    <td class="" style="width: 50%;border-left:1px solid #000">
                        <b class="nameCol padding2" style="float: left">Billing Cycle (Pone)</b>
                        <div class="greyButton">Monthly</div>
                        <div class="greyButton">Quarterly</div>
                    </td>
                </tr>
                <tr class="fullWrap rowBorder headBgColor">
                    <td class="padding2">
                        <b>Document Attached</b>
                    </td>
                </tr>
                <tr class="fullWrap rowBorder">
                    <td class="padding2" style="width: 50%">
                        <b>Address Proof</b>
                    </td>
                    <td class="padding2" style="width: 50%;border-left:1px solid #000">
                        <b>Identity Proof</b>
                    </td>
                </tr>
                <tr class="fullWrap rowBorder">
                    <td class="padding2" style="width: 50%">
                        <b>Registration of Company/Firm</b>
                    </td>
                    <td class="padding2" style="width: 50%;border-left:1px solid #000">
                        <b>Authorised Signatory Proof</b>
                    </td>
                </tr>
                <tr class="fullWrap rowBorder">
                    <td class="nameCol padding2">
                        <b>PAN/AADHAR/PASSPORT</b>
                    </td>
                    <td>
                        <ul class="fillBox">
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ul>
                    </td>
                </tr>
                <tr class="fullWrap rowBorder headBgColor">
                    <td class="padding2" style="width:100%;">
                        <b style="width:100%;text-align: center;float: left">Customer Acceptance</b>
                    </td>
                </tr>
            </tbody>
        </table>
        <table class="fullWrap">
            <tbody class="fullWrap">
                <tr class="fullWrap">
                    <td>Signature of the Authorized person with Company Seal</td>
                </tr>
                <tr class="fullWrap">
                    <td>
                        <div class="signWrap"></div>
                    </td>
                    <td class="pullRight" style="width: calc(100% - 210px)">
                        <ul class="bottomList">
                            <li>Authorized Person Name ........................</li>
                            <li>Date ........................</li>
                            <li>Place ........................</li>
                        </ul>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</page>

</body>
</html>