<style type="text/css">.fullWrap,h1,h2,h3,h4,h5,table,tbody,td,tr{float:left}.center,.fillBox li,.passportTopText{text-align:center}a,abbr,acronym,address,applet,article,aside,audio,b,big,blockquote,body,canvas,caption,center,cite,code,dd,del,details,dfn,div,dl,dt,em,embed,fieldset,figcaption,figure,footer,form,h1,h2,h3,h4,h5,h6,header,hgroup,html,i,iframe,img,ins,kbd,label,legend,li,mark,menu,nav,object,ol,output,p,pre,q,ruby,s,samp,section,small,span,strike,strong,sub,summary,sup,table,tbody,td,tfoot,th,thead,time,tr,tt,u,ul,var,video{margin:0;padding:0;border:0;font:inherit;vertical-align:baseline}.passportPhoto,.rowBorder,.srfWrap{border:1px solid #000}article,aside,details,figcaption,figure,footer,header,hgroup,menu,nav,section{display:block}body{line-height:130%;font-family:Arial,sans-serif;font-size:12px}*{box-sizing:border-box}a{text-decoration:none;color:#3D4DAC}ol,ul{list-style:none}blockquote,q{quotes:none}blockquote:after,blockquote:before,q:after,q:before{content:'';content:none}table{border-collapse:collapse;border-spacing:0}b{font-weight:700}h5{font-size:14px;margin:3px 0}h4{font-size:16px}h3{font-size:18px}h2{font-size:20px}h1{font-size:22px}.bottomList li,.greyButton,.headerLeftWrap p{font-size:10px}.greyButton{float:left;color:#fff;padding:3px 6px 1px;background-color:#333;margin:0 5px;line-height:12px}.fullWrap{width:100%}.pullRight{float:right}.halfWrap{float:left;width:50%}.padding10{padding:10px}.padding2{padding:2px}.headerLeftWrap{float:left;width:300px}.midWrap{width:595px;margin:auto}.logoWrap{margin:auto;width:80px;height:30px;background-color:#ccc}.headerLeftWrap h5,.headerLeftWrap p,.srfWrap{width:100%;float:left}.srfWrap{margin-top:5px}.srfWrap span{float:left;padding:5px 15px;background-color:#000;color:#fff;line-height:9px}.headerRightWrap{float:right;width:100px}.passportPhoto{float:left;width:80px;height:100px;margin:0 10px}.fillBox,.passportTopText{width:100%;float:left}.rowBorder{border-bottom:none}.borderBottom,.formWrap tr:last-child{border-bottom:1px solid #000}.fillBox li{float:left;width:15px;height:16px;border-left:1px solid #000;padding:3px 0}.nameCol{float:left;width:170px;line-height:12px}.rightColWidth{width:calc(100% - 170px)}.headBgColor{background-color:#ccc}.signWrap{float:left;width:200px;height:50px;border:1px solid #000}.bottomList{float:right;margin-top:35px}.bottomList li{float:left;margin-left:5px}.addressBox li:nth-child(-n+56){border-bottom:1px solid #000;}</style>    

    <div class="midWrap">
        <table class="fullWrap">
            <tbody class="fullWrap">
                <tr class="padding10 fullWrap">
                    <td class="headerLeftWrap">
                        <div class="logoWrap"></div>
                        <b class="fullWrap" style="margin-top:5px;">HANSIN IT SERVICES PVT. LTD</b>
                        <p>
                            6th Floor, Malik Plaza, Opp. S.D. College, G.T. Road,
                            <br> Panipat-132103 (Haryana) Tel.: +91 180 - 2631111,
                            <br> E-Mail:- sales@hansinit.com, www.hansinit.com
                        </p>
                        <div class="srfWrap">
                            <span>SRF / CRF No. :</span>
                        </div>
                    </td>
                    <td class="headerRightWrap">
                        <b class="passportTopText">(Class - C ISP)</b>
                        <div class="passportPhoto"></div>
                    </td>
                </tr>
            </tbody>
        </table>
        <table class="fullWrap formWrap">
            <tbody class="fullWrap">
                <tr class="padding2 fullWrap">
                    <td class="fullWrap">
                        <h3 class="fullWrap center">
                            <b>Customer Application Form (CAF)</b>
                        </h3>
                    </td>
                </tr>
                <tr class="fullWrap rowBorder headBgColor">
                    <td class="padding2">
                        <b>General Information</b>
                    </td>
                </tr>
                <tr class="fullWrap rowBorder">
                    <td class="nameCol padding2">
                        <b>Name</b>
                    </td>
                    <td>
                        <ul class="fillBox">
                            <li>T</li>
                            <li>E</li>
                            <li>S</li>
                            <li>T</li>
                            <li></li>
                            <li>N</li>
                            <li>A</li>
                            <li>M</li>
                            <li>E</li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ul>
                    </td>
                </tr>
                <tr class="fullWrap rowBorder">
                    <td class="nameCol padding2">
                        <b>Father's & Husband Name</b>
                    </td>
                    <td>
                        <ul class="fillBox">
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ul>
                    </td>
                </tr>
                <tr class="fullWrap rowBorder">
                    <td class="nameCol padding2">
                        <b>Subscriber</b>
                    </td>
                    <td>
                        <div class="greyButton">Corporate/SME</div>
                        <div class="greyButton">Institutional</div>
                        <div class="greyButton">Individual/Home </div>
                        <div class="greyButton">Organization</div>
                    </td>
                </tr>
                <tr class="fullWrap rowBorder">
                    <td class="nameCol padding2">
                        <b>Billing Address</b>
                    </td>
                    <td class="rightColWidth">
                        <ul class="fillBox addressBox" >
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ul>
                    </td>
                </tr>
                <tr class="fullWrap rowBorder">
                    <td class="nameCol padding2">
                        <b>Installation Address</b>
                    </td>
                    <td class="rightColWidth">
                        <ul class="fillBox addressBox">
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ul>
                    </td>
                </tr>
                <tr class="fullWrap rowBorder">
                    <td class="nameCol padding2">
                        <b>Designation</b>
                    </td>
                    <td>
                        <ul class="fillBox">
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ul>
                    </td>
                </tr>
                <tr class="fullWrap rowBorder">
                    <td class="nameCol padding2">
                        <b>Contact No.</b>
                    </td>
                    <td>
                        <ul class="fillBox">
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ul>
                    </td>
                </tr>
                <tr class="fullWrap rowBorder">
                    <td class="nameCol padding2">
                        <b>Alternate No.</b>
                    </td>
                    <td>
                        <ul class="fillBox">
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ul>
                    </td>
                </tr>
                <tr class="fullWrap rowBorder">
                    <td class="nameCol padding2">
                        <b>Email ID</b>
                    </td>
                    <td>
                        <ul class="fillBox">
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ul>
                    </td>
                </tr>
                
                <tr class="fullWrap rowBorder headBgColor">
                    <td class="padding2">
                        <b>Company Details (For Corporate Subscriber)</b>
                    </td>
                </tr>
                <tr class="fullWrap rowBorder">
                    <td class="nameCol padding2">
                        <b>Company Name</b>
                    </td>
                    <td>
                        <ul class="fillBox">
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ul>
                    </td>
                </tr>
                <tr class="fullWrap rowBorder">
                    <td class="nameCol padding2">
                        <b>Company Address</b>
                    </td>
                    <td class="rightColWidth">
                        <ul class="fillBox addressBox">
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>   
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ul>
                    </td>
                </tr>
                <tr class="fullWrap rowBorder">
                    <td class="nameCol padding2">
                        <b>GST NO.</b>
                    </td>
                    <td>
                        <ul class="fillBox">
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ul>
                    </td>
                </tr>
                <tr class="fullWrap rowBorder headBgColor">
                    <td class="padding2">
                        <b>Plan Details</b>
                    </td>
                </tr>
                <tr class="fullWrap rowBorder">
                    <td class="nameCol padding2">
                        <b>Service Name</b>
                    </td>
                    <td>
                        <ul class="fillBox">
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ul>
                    </td>
                </tr>
                <tr class="fullWrap rowBorder">
                    <td class="nameCol padding2">
                        <b>Plan Name</b>
                    </td>
                    <td>
                        <ul class="fillBox">
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ul>
                    </td>
                </tr>
                <tr class="fullWrap rowBorder">
                    <td class="nameCol padding2">
                        <b>Plan Amount</b>
                    </td>
                    <td>
                        <ul class="fillBox">
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ul>
                    </td>
                </tr>
                
                <tr class="fullWrap rowBorder headBgColor">
                    <td class="padding2">
                        <b>Other Charges</b>
                    </td>
                </tr>
                <tr class="fullWrap rowBorder">
                    <td class="nameCol padding2">
                        <b>Charge Name</b>
                    </td>
                    <td>
                        <ul class="fillBox">
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ul>
                    </td>
                </tr>
            </tbody>
        </table>
        <table style="margin-bottom:20px;">
            <tbody>
                <tr class="fullWrap">
                    <td>Signature of the Authorized person with Company Seal</td>
                </tr>
                <tr class="fullWrap">
                    <td>
                        <div class="signWrap"></div>
                    </td>
                    <td class="pullRight" style="width: calc(100% - 210px)">
                        <ul class="bottomList">
                            <li>Authorized Person Name ........................</li>
                            <li>Date ....................</li>
                            <li>Place ....................</li>
                        </ul>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>


</body>

</html>