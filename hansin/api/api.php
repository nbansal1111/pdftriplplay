<?php 

// calling API function
function CallAPI($method, $url, $token, $data = false){
    $curl = curl_init();
    switch ($method)
    {
        case "POST":
            curl_setopt($curl, CURLOPT_POST, 1);
            if ($data)
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            break;
        case "PUT":
            curl_setopt($curl, CURLOPT_PUT, 1);
            break;
        default:
            if ($data)
                $url = sprintf("%s?%s", $url, http_build_query($data));
    }
    // Authentication:
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    // curl_setopt($curl, CURLOPT_USERPWD, "username:password");
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'token:' . $token
        ));
    $result = curl_exec($curl);
    curl_close($curl);
    return $result;
}
function callPDFGenerator($resJsonData, $resJsonObj){
		// calling PDF Generator file
		include_once '../index.php';
	
}

// Get Call

	$resBody = array('error'=>false,'message'=>'Success');

	if ($_SERVER['REQUEST_METHOD'] == 'GET') {

		// checking ID and token exists in URL
		$isID = array_key_exists('id', $_GET);
		$isToken = array_key_exists('token', $_GET);
		if ($isID == 1 && $isToken == 1) {
			// calling customer Data API
			$cusID = $_GET['id'];
			$cusToken = $_GET['token'];
			$url = 'http://default-environment.dnvv7k2pbr.ap-south-1.elasticbeanstalk.com/tripleplay/customers/'.$cusID;
			$cusDataRes = CallAPI('GET', $url, $cusToken);
			$resJson = json_decode($cusDataRes, true);
			// print_r($resJson['data']);
			$resJsonData = $resJson['data'];
			// loop for data object array and call PDF gen
			for ($i=0; $i < sizeof($resJsonData); $i++) { 
				// single obj
				$resJsonObj = $resJsonData[$i];
				callPDFGenerator($resJsonData, $resJsonObj);
				break;
			}

			// echo $cusDataRes->statusCode;
			// echo json_encode($resBody);	
			http_response_code(200);
		}else{
			$resBody['error'] = "true";
			$resBody['message'] = "Check your parameters";
			echo json_encode($resBody);
		}
	}else{
		http_response_code(405);
	}





?>