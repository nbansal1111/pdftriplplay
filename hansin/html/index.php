<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="index.css">
    <title>Hansin</title>
</head>

<body>
    <?php 
        $jsonUrl = file_get_contents('http://localhost/hansin/data.json');
        $json = json_decode($jsonUrl, true); 
        // echo $json;
    ?>
    <div class="midWrap">
        <table class="fullWrap">
            <tbody class="fullWrap">
                <tr class="padding10 fullWrap">
                    <td class="headerLeftWrap">
                        <div class="logoWrap"></div>
                        <b class="fullWrap" style="margin-top:5px;">HANSIN IT SERVICES PVT. LTD</b>
                        <p>
                            6th Floor, Malik Plaza, Opp. S.D. College, G.T. Road,
                            <br> Panipat-132103 (Haryana) Tel.: +91 180 - 2631111,
                            <br> E-Mail:- sales@hansinit.com, www.hansinit.com
                        </p>
                        <div class="srfWrap">
                            <span>SRF / CRF No. :</span>
                        </div>
                    </td>
                    <td class="headerRightWrap">
                        <b class="passportTopText">(Class - C ISP)</b>
                        <div class="passportPhoto"></div>
                    </td>
                </tr>
            </tbody>
        </table>
        <table class="fullWrap formWrap">
            <tbody class="fullWrap">
                <tr class="padding2 fullWrap">
                    <td class="fullWrap">
                        <h3 class="fullWrap center">
                            <b>Customer Application Form (CAF)</b>
                        </h3>
                    </td>
                </tr>
                <tr class="fullWrap rowBorder headBgColor">
                    <td class="padding2">
                        <b>General Information</b>
                    </td>
                </tr>
                <tr class="fullWrap rowBorder">
                    <td class="nameCol padding2">
                        <b>Subscriber Name</b>
                    </td>
                    <td>
                        <ul class="fillBox">
                            <li>R</li>
                            <li>A</li>
                            <li>J</li>
                            <li>N</li>
                            <li>I</li>
                            <li>S</li>
                            <li>H</li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ul>
                    </td>
                </tr>
                <tr class="fullWrap rowBorder">
                    <td class="nameCol padding2">
                        <b>Father's & Husband Name</b>
                    </td>
                    <td>
                        <ul class="fillBox">
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ul>
                    </td>
                </tr>
                <tr class="fullWrap rowBorder">
                    <td class="nameCol padding2">
                        <b>Subscriber Type (Pone)</b>
                    </td>
                    <td>
                        <div class="greyButton">Corporate/SME</div>
                        <div class="greyButton">Institutional</div>
                        <div class="greyButton">Individual/Home </div>
                        <div class="greyButton">Organization</div>
                    </td>
                </tr>
                <tr class="fullWrap rowBorder">
                    <td class="nameCol padding2">
                        <b>Local Address
                            <br>(For Billing & Termination)</b>
                    </td>
                    <td class="rightColWidth">
                        <ul class="fillBox" style="border-bottom:1px solid #000;">
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ul>
                        <ul class="fillBox" style="border-bottom:1px solid #000;">
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ul>
                        <ul class="fillBox">
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ul>
                    </td>
                </tr>
                <tr class="fullWrap rowBorder">
                    <td class="nameCol padding2">
                        <b>Wired Line Phone No.</b>
                    </td>
                    <td>
                        <ul class="fillBox">
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ul>
                    </td>
                    <td class="nameCol padding2" style="width: 75px;">
                        <b>Mobile No.</b>
                    </td>
                    <td>
                        <ul class="fillBox">
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ul>
                    </td>
                </tr>
                <tr class="fullWrap rowBorder">
                    <td class="nameCol padding2">
                        <b>Address Type (Pone)</b>
                    </td>
                    <td>
                        <div class="greyButton">Owned</div>
                        <div class="greyButton">Rented</div>
                    </td>
                    <td style="float:right;">
                        mytestemail@gmail.com
                    </td>
                    <td class="nameCol padding2" style="float:right;width: 60px;border-right:1px solid #000;margin-right: 10px;">
                        <b>E-mail</b>
                    </td>
                </tr>
                <tr class="fullWrap rowBorder">
                    <td class="nameCol padding2">
                        <b>Contact Details of Landlord</b>
                    </td>
                    <td>
                        <ul class="fillBox">
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ul>
                    </td>
                </tr>
                <tr class="fullWrap rowBorder">
                    <td class="nameCol padding2">
                        <b>Wired Line Phone No.</b>
                    </td>
                    <td>
                        <ul class="fillBox">
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ul>
                    </td>
                    <td class="nameCol padding2" style="width: 75px;">
                        <b>Mobile No.</b>
                    </td>
                    <td>
                        <ul class="fillBox">
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ul>
                    </td>
                </tr>
                <tr class="fullWrap rowBorder headBgColor">
                    <td class="padding2">
                        <b>Administrative Contact Information (For Corporate Subscriber)</b>
                    </td>
                </tr>
                <tr class="fullWrap rowBorder">
                    <td class="nameCol padding2">
                        <b>Owner/Director/Manager</b>
                    </td>
                    <td>
                        <ul class="fillBox">
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ul>
                    </td>
                </tr>
                <tr class="fullWrap rowBorder">
                    <td class="nameCol padding2">
                        <b>Residence Address</b>
                    </td>
                    <td class="rightColWidth">
                        <ul class="fillBox" style="border-bottom:1px solid #000;">
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ul>
                        <ul class="fillBox">
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ul>
                    </td>
                </tr>
                <tr class="fullWrap rowBorder">
                    <td class="nameCol padding2">
                        <b>Wired Line Phone No.</b>
                    </td>
                    <td>
                        <ul class="fillBox">
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ul>
                    </td>
                    <td class="nameCol padding2" style="width: 75px;">
                        <b>Mobile No.</b>
                    </td>
                    <td>
                        <ul class="fillBox">
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ul>
                    </td>
                </tr>
                <tr class="fullWrap rowBorder headBgColor">
                    <td class="padding2">
                        <b>Customer Co-ordinator Information (For Corporate Subscriber)</b>
                    </td>
                </tr>
                <tr class="fullWrap rowBorder">
                    <td class="nameCol padding2">
                        <b>Name</b>
                    </td>
                    <td>
                        <ul class="fillBox">
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ul>
                    </td>
                </tr>
                <tr class="fullWrap rowBorder">
                    <td class="nameCol padding2">
                        <b>Designation</b>
                    </td>
                    <td>
                        <ul class="fillBox">
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ul>
                    </td>
                </tr>
                <tr class="fullWrap rowBorder">
                    <td class="nameCol padding2">
                        <b>Wired Line Phone No.</b>
                    </td>
                    <td>
                        <ul class="fillBox">
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ul>
                    </td>
                    <td class="nameCol padding2" style="width: 75px;">
                        <b>Mobile No.</b>
                    </td>
                    <td>
                        <ul class="fillBox">
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ul>
                    </td>
                </tr>
                <tr class="fullWrap rowBorder">
                    <td class="nameCol padding2">
                        <b>Fax</b>
                    </td>
                    <td>
                        <ul class="fillBox">
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ul>
                    </td>
                    <td style="float:right;">
                        mytestemail@gmail.com
                    </td>
                    <td class="nameCol padding2" style="float:right;width: 60px;border-right:1px solid #000;margin-right: 10px;">
                        <b>E-mail</b>
                    </td>
                </tr>
                <tr class="fullWrap rowBorder headBgColor">
                    <td class="padding2">
                        <b>Customer Billing Address (If Different to above) </b>
                    </td>
                </tr>
                <tr class="fullWrap rowBorder">
                    <td class="nameCol padding2">
                        <b>Contact Person Name</b>
                    </td>
                    <td>
                        <ul class="fillBox">
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ul>
                    </td>
                </tr>
                <tr class="fullWrap rowBorder">
                    <td class="nameCol padding2">
                        <b>Residence Address</b>
                    </td>
                    <td class="rightColWidth">
                        <ul class="fillBox" style="border-bottom:1px solid #000;">
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ul>
                        <ul class="fillBox">
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ul>
                    </td>
                </tr>
                <tr class="fullWrap rowBorder">
                    <td class="nameCol padding2">
                        <b>Wired Line Phone No.</b>
                    </td>
                    <td>
                        <ul class="fillBox">
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ul>
                    </td>
                    <td class="nameCol padding2" style="width: 75px;">
                        <b>Mobile No.</b>
                    </td>
                    <td>
                        <ul class="fillBox">
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ul>
                    </td>
                </tr>
                <tr class="fullWrap rowBorder headBgColor">
                    <td class="padding2">
                        <b>Service Details </b>
                    </td>
                </tr>
                <tr class="fullWrap rowBorder">
                    <td class="nameCol padding2">
                        <b>Required Service (Pone)</b>
                    </td>
                    <td>
                        <div class="greyButton">Fixed Wi-Fi Broadband</div>
                        <div class="greyButton">Broadband on Cable</div>
                        <div class="greyButton">DIA-LL(Direct Internet Access )</div>
                    </td>
                </tr>
                <tr class="fullWrap rowBorder">
                    <td class="nameCol padding2">
                        <b>Service Delivery (Pone)</b>
                    </td>
                    <td>
                        <div class="greyButton">On Fixed Wi-Fi CPE</div>
                        <div class="greyButton">Copper / Ethernet Cable</div>
                        <div class="greyButton">Optical Fiber (Fiber to Home) </div>
                    </td>
                </tr>
                <tr class="fullWrap rowBorder">
                    <td class="nameCol padding2">
                        <b>Plan & Speed</b>
                    </td>
                    <td style="width: 150px;line-height:130%;">
                        As per plan sheet
                    </td>
                    <td class="nameCol padding2" style="width: 160px;border-right:1px solid #000;margin-right: 10px;">
                        <b>Required Static Public IP</b>
                    </td>
                    <td style="line-height:130%">
                        test IP
                    </td>
                </tr>
                <tr class="fullWrap rowBorder">
                    <td class="nameCol padding2">
                        <b>Traffic Limit (UL/DL)</b>
                    </td>
                    <td>
                        <b> As per Fair Uses Policy & as per Tariff Plan</b>
                    </td>
                </tr>
                <tr class="fullWrap rowBorder headBgColor">
                    <td class="padding2">
                        <b>Financial Charges Details</b>
                    </td>
                </tr>
                <tr class="fullWrap rowBorder">
                    <td class="padding2" style="width: 50%">
                        <b>One Time Activation (Non refundable)</b>
                    </td>
                    <td class="padding2" style="width: 50%;border-left:1px solid #000">
                        <b> Security Deposit (Refundable)</b>
                    </td>
                </tr>
                <tr class="fullWrap rowBorder">
                    <td class="padding2" style="width: 50%">
                        <b>CPE Cost (Non refundable)</b>
                    </td>
                    <td class="padding2" style="width: 50%;border-left:1px solid #000">
                        <b>CPE Rental (Non refundable)</b>
                    </td>
                </tr>
                <tr class="fullWrap rowBorder">
                    <td class="" style="width: 50%">
                        <b class="nameCol padding2" style="float: left">Subscription Type (Pone)</b>
                        <div class="greyButton">Prepaid</div>
                        <div class="greyButton">Post Pay</div>
                    </td>
                    <td class="" style="width: 50%;border-left:1px solid #000">
                        <b class="nameCol padding2" style="float: left">Billing Cycle (Pone)</b>
                        <div class="greyButton">Monthly</div>
                        <div class="greyButton">Quarterly</div>
                    </td>
                </tr>
                <tr class="fullWrap rowBorder headBgColor">
                    <td class="padding2">
                        <b>Document Attached</b>
                    </td>
                </tr>
                <tr class="fullWrap rowBorder">
                    <td class="padding2" style="width: 50%">
                        <b>Address Proof</b>
                    </td>
                    <td class="padding2" style="width: 50%;border-left:1px solid #000">
                        <b>Identity Proof</b>
                    </td>
                </tr>
                <tr class="fullWrap rowBorder">
                    <td class="padding2" style="width: 50%">
                        <b>Registration of Company/Firm</b>
                    </td>
                    <td class="padding2" style="width: 50%;border-left:1px solid #000">
                        <b>Authorised Signatory Proof</b>
                    </td>
                </tr>
                <tr class="fullWrap rowBorder">
                    <td class="nameCol padding2">
                        <b>PAN/AADHAR/PASSPORT</b>
                    </td>
                    <td>
                        <ul class="fillBox">
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                            <li></li>
                        </ul>
                    </td>
                </tr>
                <tr class="fullWrap rowBorder headBgColor">
                    <td class="padding2" style="width:100%;">
                        <b style="width:100%;text-align: center;float: left">Customer Acceptance</b>
                    </td>
                </tr>
            </tbody>
        </table>
        <table style="margin-bottom:20px;">
            <tbody>
                <tr class="fullWrap">
                    <td>Signature of the Authorized person with Company Seal</td>
                </tr>
                <tr class="fullWrap">
                    <td>
                        <div class="signWrap"></div>
                    </td>
                    <td class="pullRight" style="width: calc(100% - 210px)">
                        <ul class="bottomList">
                            <li>Authorized Person Name ........................</li>
                            <li>Date ........................</li>
                            <li>Place ........................</li>
                        </ul>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>


</body>

</html>