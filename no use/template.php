<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Document</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="http://localhost/timbl/register-form.css">
</head>

<?php

// parsing data
$jsonFile = file_get_contents('http://localhost/timbl/data.json');
$json = json_decode($jsonFile);

// customer details list
// $customerFirstName = $json->customerDetails->customerType->Individual;
$customerTypeSelected = $json->customerDetails->customerType;
$serviceTypeSelected = $json->servicesDetails->serviceType;

$customerNameInformation = $json->customerDetails->customerName;
$customerFirstName = $customerNameInformation->firstName;
$customerMiddleName = $customerNameInformation->middleName;
$customerLastName = $customerNameInformation->lastName;

$AuthorizedInformation = $json->customerDetails->authorizedName;
$AuthorizedFirstName = $AuthorizedInformation->firstName;
$AuthorizedMiddleName = $AuthorizedInformation->middleName;
$AuthorizedLastName = $AuthorizedInformation->lastName;
$autorizedDesignation = $json->customerDetails->autorizedDesignation;

$coordinatorInformation = $json->customerDetails->coordinatorName;
$coordinatorFirstName = $coordinatorInformation->firstName;
$coordinatorMiddleName = $coordinatorInformation->middleName;
$coordinatorLastName = $coordinatorInformation->lastName;
$coordinatorMobileNumber = $coordinatorInformation->mobileNumber;
$coordinatorEmailId = $coordinatorInformation->emailId;
$customerGender = $json->customerDetails->gender;
$customerFatherName = $json->customerDetails->fatherName;
$customerBirthDate = $json->customerDetails->birthDate;

$customerNationality = $json->customerDetails->nationality;
$customerInformation = $json->customerDetails->localReferenceDetails->passportDetails;
$customerPassportNo = $customerInformation->passportNo;
$customerPassportValidUpto = $customerInformation->passportValidUpto;
$customerVisaType = $customerInformation->visaType;
$customerVisaValidUpto = $customerInformation->visaValidUpto;

$localInformation = $json->customerDetails->localReferenceDetails->name;
$localFirstName = $localInformation->firstName;
$localMiddleName = $localInformation->middleName;
$localLastName = $localInformation->lastName;

$localInfo = $json->customerDetails->localReferenceDetails;
$customerAddress = $localInfo->address;
$customerMobileNo = $localInfo->mobileNo;
$customerPanNo = $localInfo->panNo;

$customerAadharNo = $json->customerDetails->aadharNo;
$customerRegisteredNumber = $json->customerDetails->registeredNumber;

$billingInformation = $json->customerDetails->billingAddress;
$billingCity = $billingInformation->city;
$billingLandmark = $billingInformation->landmark;
$billingState = $billingInformation->state;
$billingPincode = $billingInformation->pincode;
$billingContactNo = $billingInformation->contactNo;

$customerPropertyType = $json->customerDetails->propertyType;
$customerLastName = $json->customerDetails->autorizedDesignation;

// orde payment 
$orderTypeofservice = $json->orderPaymentDetails->typeOfService;
$planInformation = $json->orderPaymentDetails->planDetails;
$planName = $planInformation->name;
$planAddonValue = $planInformation->addonValue;
$planValidity = $planInformation->validity;
$orderBillType = $json->orderPaymentDetails->billType;
$orderApplicationType = $json->orderPaymentDetails->applicationType;
$paymentInformation = $json->orderPaymentDetails->paymentDetails;
$paymentRegistrationCharges = $paymentInformation->registrationCharges;
$paymentActiveCharges = $paymentInformation->activeCharges;
$paymentSecurityDeposit = $paymentInformation->securityDeposit;
$paymentPlanCharge = $paymentInformation->planCharge;
$paymentTotal = $paymentInformation->total;
$paymentPaymentMode = $paymentInformation->paymentMode;
$paymentReceiptNo = $paymentInformation->receiptNo;
$paymentDate = $paymentInformation->date;
$paymentChequeDD = $paymentInformation->chequeDD;
$paymentBank = $paymentInformation->bank;
$paymentBranch = $paymentInformation->branch;
$paymentCity = $paymentInformation->city;
$paymentOnlinePayment = $paymentInformation->onlinePayment;
$paymentTransactionID = $paymentInformation->transactionID;

//document
$documentNameAndAddress = $json->documents->nameAndAddress;
$documentIdProof = $json->documents->idProof;
$documentAddressProof = $json->documents->addressProof;
$documentAmountOfTransaction = $json->documents->amountOfTransaction;
$documentParticularOfTransaction = $json->documents->particularOfTransaction;
$documentAssessedTax = $json->documents->assessedTax;
$documentLastIncomeReturn = $json->documents->lastIncomeReturn;
$documentRegistrationNumber = $json->documents->registrationNumber;
$documentCountriesAddress = $json->documents->countriesAddress;
$documentVerification = $json->documents->verification;
$documentDate = $json->documents->date;
$documentPlace = $json->documents->place;

//servicesDetails
$serviceInfo = $json->servicesDetails;
$serviceRefferalCode = $serviceInfo->refferalCode;
$serviceCafSignDate = $serviceInfo->cafSignDate;
$serviceSrName = $serviceInfo->srName;
$serviceSrCode = $serviceInfo->srCode;
$serviceBpName = $serviceInfo->bpName;
$serviceBpCode = $serviceInfo->bpCode;
$serviceSignatureSR = $serviceInfo->signatureSR;
$serviceAgentAuthTXNID = $serviceInfo->agentAuthTXNID;
$serviceAgentAuthTS = $serviceInfo->agentAuthTS;
$serviceEkycTXNID = $serviceInfo->ekycTXNID;
$serviceEkycTimeStamp = $serviceInfo->ekycTimeStamp;
$serviceCustomerAuthTXNID = $serviceInfo->customerAuthTXNID;
$serviceCustomerAuthTS = $serviceInfo->customerAuthTS;

// document object
// $paymentTransactionID = $json->documents->paymentDetails->transactionID;
//print_r($json);
//print_r($customerFirstName);
// print($json);

?>

	<body>
		<table style="width:595px; float: left;">
			<tbody style="float: left; width: 100%;">
				<tr>
					<td>
						<div style="width: 100px; float: left; margin-right: 180px;">
							<img src="https://i.imgur.com/X8OQ4TH.png" width="150px;" height="100px;">
						</div>
					</td>
					<td>
						<div style="float: left; width: 280px; margin-left: 13%;">
							<p style="font-size:20px; font-size: 20px; margin-left: -50px;">
								<strong>Customer Agreement Form</strong>
							</p>
							<p style="margin-left: -20px; margin-top: -14px;">
								<strong>(To be filled in block letters)</strong>
							</p>

						</div>
					</td>
					<td>
						<div style="float: left; width: 280px; margin-left: 51%;">
							<p style="font-size: 12px;">Contact Center: 92121-99966</p>
							<p style="font-size: 12px; margin-top: -11px; margin-left: 29px;">CAF Number: 21000091</p>
						</div>
					</td>
				</tr>
				<tr style="float: left; width: 100%;">
					<td style="float: left; width: 100%;">
						<div style="background-color: #232020;  border-radius: 3px;">
							<div style="text-align: center; color: white;">
								<p>Customer Details</p>
							</div>
						</div>
					</td>
				</tr>
				<tr style="float: left; width: 100%;">
					<td style="float: left; width: 100%;">
						<div style="font-size: 12px; margin-left: 3px; float: left;">
							<?php for ($i=0; $i < sizeOf($customerTypeSelected) ; $i++) { ?> 
								<input id="Individual" style="float: left;" type="checkbox"
								<?php if($customerTypeSelected[$i]->selected) echo "checked='checked'"; ?> >
								<span style="margin-right: 8px; font-size: 11px; float: left; margin-top: 4px;" id="Proprietorship">
									<?php print_r($customerTypeSelected[$i]->type); ?>
								</span>
							<?php } ?>
						</div>
					</td>
				</tr>
				<tr style="float: left; width: 100%;">
					<td style="width:710px; float: left;">
						<!-- Individual/Organization -->
						<ul style="float: left; width: 100%; list-style: none; padding: 0; margin: 0; margin-top: 10px;">
							<li style="margin-top: -8px; float: left; width: 165px; margin-right: 10px;">
								<p style="width: 100%; text-align: center; font-size: 11px; margin-left: -18px; margin-top: 10px!important;">1.Individual/Organization:</p>
							</li>
							<li style="float: left; width: 165px; margin-right: 10px; width: 165px; height: 22px; border: 1px solid darkgray;">
								<?php print($customerFirstName); ?>
								<p style="width: 100%; text-align: center; font-size: 11px;">First name</p>
							</li>
							<li style="float: left; width: 165px; margin-right: 10px; width: 165px; height: 22px; border: 1px solid darkgray;">
							<?php print($customerMiddleName); ?>
								<p style="width: 100%; text-align: center; font-size: 11px;">Middle name</p>
							</li>
							<li style="float: left; width: 165px; margin-right: 10px; width: 165px; height: 22px; border: 1px solid darkgray;">
							<?php print($customerLastName); ?>
								<p style="width: 100%; text-align: center; font-size: 11px;">Last name</p>
							</li>
						</ul>
						<!-- Individual/Organization -->
						<ul style="float: left; width: 100%; list-style: none; padding: 0; margin: 0; margin-top: 10px;">
							<li style="margin-top: -8px; float: left; width: 165px; margin-right: 10px;">
								<p style="width: 100%; text-align: center; font-size: 11px; margin-top: 3px; margin-left: -10px;">Authorised Signatory Name:</p>
								<p style="margin-top: -10px; width: 100%; text-align: center; font-size: 11px; font-size: 10px; margin-left: -10px;">(NA for Individual/Proprietorship)</p>
							</li>
							<li style="float: left; width: 165px; margin-right: 10px; width: 165px; height: 22px; border: 1px solid darkgray;">
								<?php print($AuthorizedFirstName); ?>
								<p style="width: 100%; text-align: center; font-size: 11px;">First name</p>
							</li>
							<li style="float: left; width: 165px; margin-right: 10px; width: 165px; height: 22px; border: 1px solid darkgray;">
							<?php print($AuthorizedMiddleName); ?>
								<p style="width: 100%; text-align: center; font-size: 11px;">Middle name</p>
							</li>
							<li style="float: left; width: 165px; margin-right: 10px; width: 165px; height: 22px; border: 1px solid darkgray;">
							<?php print($AuthorizedLastName); ?>
								<p style="width: 100%; text-align: center; font-size: 11px;">Last name</p>
							</li>
						</ul>
						<!--  Authorised Signatory Designation:    -->
						<ul style="float: left; width: 100%; list-style: none; padding: 0; margin: 0; margin-top: 10px;">
							<li style="margin-top: -8px; float: left; width: 165px; margin-right: 10px;">
								<p style="width: 100%; text-align: center; font-size: 11px; margin-top: 3px; margin-left: 2px;">Authorised Signatory Designation:</p>
							</li>
							<li style="float: left; margin-right: 10px; width: 514px; height: 22px; border: 1px solid darkgray;">
							<?php print($autorizedDesignation); ?>
							</li>
						</ul>
						<ul class="ulRow">
							<li>
								<p class="txt-size" style="margin-top: 3px; margin-left: -2px;">Organisation Co­ordinator Name:</p>
								<p class="margin10px" style="font-size: 10px; margin-left: -4px;">(NA for Individual/Proprietorship)</p>
							</li>
							<li style="float: left;" class="input-box">
							<?php print($coordinatorFirstName); ?>
								<p>First name</p>
							</li>
							<li style="float: left;" class="input-box">
							<?php print($coordinatorMiddleName); ?>
								<p>Middle name</p>
							</li>
							<li style="float: left;" class="input-box">
							<?php print($coordinatorLastName); ?>
								<p>Last name</p>
							</li>
						</ul>
						<ul class="ulRow">
							<li style="width: 165px;">
								<p class="txt-size" style="margin-top: 3px;">Mobile No:</p>
							</li>
							<li style="float: left;" class="input-box">
							<?php print($coordinatorMobileNumber); ?>
							</li>
							<li style="width: 48px;">
								<p class="txt-size" style="margin-top: 3px;">E­mail ID:</p>
							</li>
							<li style="float: left;" class="email-box">
							<?php print($coordinatorEmailId); ?>
								<p style="font-size: 10px;">(Your postpaid bill/Statements will be maild on above mentioned E­mail ID)</p>
							</li>
						</ul>
					</td>
					<td style="float:left; margin-top: 10px;">
						<img src="https://i.imgur.com/17PEIZU.jpg" width="160px" ; height="180px" ; />
					</td>
				</tr>

				<tr class="trRow">
					<td style="float: left;">
						<p style="float: left;" class="txt-size bottom-space">2.Father's/Husband's Name: </p>
					</td>
				</tr>
				<tr class="trRow">
					<td class="father-box">
					<?php print($customerFatherName); ?>
					</td>
				</tr>
				<tr style="float: left; width: 100%;">
					<td style="float: left; width:450px;">
						<ul class="ulRow">
							<li style="margin-top: -7px; width: 53px;">
								<p style="float: left;" class="txt-size bottom-space">3.Birth Date:</p>
							</li>
							<li class="birth-box">
							<?php print($customerBirthDate); ?>
							</li>
							<li style="margin-top: -7px; width: 40px; margin-left: 12px;">
								<p style="float: left;" class="txt-size bottom-space">4.Gender:</p>
							</li>
							<li style="width:23px;">

								<input id="male" style="width:13px;" type="checkbox"
								<?php if($customerGender =="Male") echo "checked='checked'"; ?> >
								<p style="margin-top: 4px;">M</p>
							</li>
							<li style="width:23px;">
								<input id="female" style="width:13px;" type="checkbox" value=""
								<?php if($customerGender =="Female") echo "checked='checked'"; ?> >
								<p style="margin-top: 4px;">F</p>
							</li>
							<li style="margin-top: -7px; width: 56px; margin-left: 10px;">
								<p style="float: left;" class="txt-size bottom-space">5.Nationality:</p>
							</li>
							<li class="nationality-box">
							<?php print($customerNationality); ?>
							</li>
							<li style="width: 307px;">
								<p style="float: left;" class="txt-size bottom-space">6. Details of Local Reference (For Outstation/Foreign National)</p>
							</li>
						</ul>
					</td>
					<td class="box" style="float: left; margin-top: 10px; padding-bottom: 10px;">
						<ul class="ulRow">
							<li style="margin-top: -7px; width: 75px;">
								<p style="float: left;" class="txt-size bottom-space text-align-right">Passport No:</p>
							</li>
							<li class="passport-box">
							<?php print($customerPassportNo); ?>
							</li>
							<li style="margin-top: -7px; width: 128px;">
								<p style="floar:left; margin-left: 21px;" class="txt-size bottom-space">Passport Valid Upto:</p>
							</li>
							<li class="passport-valid-box">
							<?php print($customerPassportValidUpto); ?>
							</li>
							<p style="font-size: 7px; width: 200px; margin-left: 11px;">
								(Passport & Visa copy required in case of Foreign national Visa Validity should not be less than 1 month)
							</p>
							<li style="margin-top: -7px; width: 75px;">
								<p style="float: left;" class="txt-size bottom-space text-align-right">Visa type:</p>
							</li>
							<li class="passport-box">
							<?php print($customerVisaType); ?>
							</li>
							<li style="margin-top: -7px; width: 128px;">
								<p style="float:left; margin-left: 21px;" class="txt-size bottom-space">Visa Valid Upto:</p>
							</li>
							<li class="passport-valid-box">
							<?php print($customerVisaValidUpto); ?>
							</li>
						</ul>
					</td>
				</tr>
				<tr style="margin-top: 10px;" class="trRow buttom3px">
					<td style="float: left;" class="leftside">
						<p style="float: left;" class="txt-size bottom-space text-align-left">Name:</p>
					</td>
					<td style="float: left;" class="input-width cust-box">
					<?php print($localFirstName); ?>
						<p>First name</p>
					</td>
					<td style="float: left;" class="input-width cust-box">
						<?php print($localMiddleName); ?>
						<p>Middle name</p>
					</td>
					<td style="float: left;" class="input-width cust-box">
						<?php print($localLastName); ?>
						<p>Last name</p>
					</td>
				</tr>
				<tr class="trRow">
					<td class="leftside">
						<p style="float: left;" class="txt-size bottom-space text-align-left">Address:</p>
					</td>
					<td class="address-box input-space">
					<?php print($customerAddress); ?>
					</td>
					<td>
						<p class="txt-size bottom-space text-align-right" style="float:left; width: 111px;">Mobile No:</p>
					</td>
					<td class="mobile-box input-space">
					<?php print($customerMobileNo); ?>
					</td>
				</tr>
				<tr class="trRow">
					<td class="leftside">
						<p style="float: left;" class="txt-size bottom-space text-align-left">PAN/GIR No:</p>
					</td>
					<td class="pan-box input-space">
					<?php print($customerPanNo); ?>
					</td>
					<td style="width:100%">
						<p class="text-align-left" style="font-size: 9px; float: left; width:100%">
							(Please provide copy of PAN/GIR No. or fill form 60/61)
						</p>
					</td>
				</tr>
				<tr class="trRow">
					<td class="leftside">
						<p style="float: left;" class="txt-size bottom-space text-align-left">8. Aadhar No:</p>
					</td>
					<td class="input-box input-space">
					<?php print($customerAadharNo); ?>
					</td>
					<td style="width: 195px;">
						<p style="float: left;" class="txt-size bottom-space text-align-right">9. Registered Mobile Number (RMN):</p>
					</td>
					<td class="reg-box input-space">
					<?php print($customerRegisteredNumber); ?>
					</td>
					<td style="width: 100%; float:left; margin-left: 18%;">
						<p style="font-size: 10px;">
							(RMN is the Number through which you can do your transactions via SMS/Call/Web Application)
						</p>
					</td>
				</tr>
				<tr class="trRow">
					<td>
						<p style="float: left;" class="txt-size bottom-space">10. Installation & billing Address:</p>
					</td>
					<td class="instal-box input-space">
					<?php print($billingCity); ?>
					</td>
					<td>
						<p class="text-align-left" style="font-size: 9px;">
							(Billing Address required only for postpaid Customers)
						</p>
					</td>
				</tr>
				<tr class="trRow">
					<td class="leftside">
						<p class="bottom-space text-align-left">City/Village/PO:</p>
					</td>
					<td class="city-box input-space">
					<?php print($billingCity); ?>
					</td>
					<td style="width: 90px;">
						<p class="bottom-space text-align-right">Landmark:</p>
					</td>
					<td class="landmark-box input-space">
					<?php print($billingLandmark); ?>
					</td>
					<td style="width: 70px;">
						<p class="bottom-space text-align-right">State:</p>
					</td>
					<td class="landmark-box input-space">
					<?php print($billingState); ?>
					</td>
				</tr>
				<tr class="trRow">
					<td>
						<p class="bottom-space text-align-left">Pincode:</p>
					</td>
					<td class="pincode-box input-space">
					<?php print($billingPincode); ?>
					</td>
					<td style="width: 200px;">
						<p class="bottom-space text-align-right">Contact No.with STD Code(if any):</p>
					</td>
					<td class="contact-box input-space">
					<?php print($billingContactNo); ?>
					</td>
					<td style="width: 135px;">
						<p class="bottom-space text-align-right">Property Details:</p>
					</td>
					<td style="width:55px; margin-top: 7px;">
						<input style="width: 13px;" type="checkbox" value=""
						<?php if($customerPropertyType =="Rented") echo "checked='checked'"; ?> >
						<span style="font-size: 11px;">Rented</span>
					</td>
					<td style="width:55px; margin-top: 7px;">
						<input style="width: 13px;" type="checkbox" value=""
						<?php if($customerPropertyType =="Owned") echo "checked='checked'"; ?>	>
						<span style="font-size: 11px;">Owned</span>
					</td>
				</tr>
				<tr style="float: left; width: 100%;">
					<td style="float: left; width: 100%; margin-top: 2%;">
						<div class="back-ground-black border-rad">
							<div style="text-align: center; color: white;">
								<p>Order & Payment Details</p>
							</div>
						</div>
					</td>
				</tr>
				<tr class="trRow">
					<td class="leftside">
						<p class="bottom-space text-align-left">11. Type of Service:</p>
					</td>
					<td style="width:60px; margin-top: 7px;">
						<input style="width: 13px;" type="checkbox" value=""
						<?php if($orderTypeofservice =="Prepaid") echo "checked='checked'"; ?> >
						<span style="font-size: 11px;">Pre-­paid</span>
					</td>
					<td style="width:60px; margin-top: 7px;">
						<input style="width: 13px;" type="checkbox" value=""
						<?php if($orderTypeofservice =="Pospaid") echo "checked='checked'"; ?>	>
						<span style="font-size: 11px;">Post­-paid</span>
					</td>
				</tr>
				<tr class="trRow">
					<td class="leftside">
						<p class="bottom-space text-align-left">12. Plan Name:</p>
					</td>
					<td class="plan-box input-space">
					<?php print($planName); ?>
					</td>
					<td style="width: 105px;">
						<p class="bottom-space text-align-right">Add­on DUL value:</p>
					</td>
					<td class="addon-box input-space">
					<?php print($planAddonValue); ?>
					</td>
					<td style="width: 250px;">
						<p class="bottom-space" style="width: 125px;">Validity(Months):</p>
						<span class="validity-box input-space">
						<?php print($planValidity); ?>
							<!-- <input type="text" class="input-space" style="width: 48%;"> -->
						</span>
						<p class="text-align-left" style="font-size: 9px; width: 100%; margin-left: 21px;">
							(Applicable only for PrePaid Customers)
						</p>
					</td>
				</tr>
				<tr class="trRow">
					<td class="leftside">
						<p class="bottom-space text-align-left">13. Bill Preference: </p>
					</td>
					<td style="width:70px; margin-top: 7px;">
						<input style="width: 13px;" type="checkbox" value=""
						<?php if($orderBillType =="PaperBill") echo "checked='checked'"; ?> >
						<span style="font-size: 11px;">Paper Bill</span>
					</td>
					<td style="width:60px; margin-top: 7px;">
						<input style="width: 13px;" type="checkbox" value=""
						<?php if($orderBillType =="EBill") echo "checked='checked'"; ?> >
						<span style="font-size: 11px;">E­-Bill</span>
					</td>
					<td style="width: 250px;">
						<p class="bottom-space text-align-left" style="text-align: right!important;">14. Type of Application/Usage of Service:</p>
					</td>
					<?php for ($i=0; $i < sizeOf($orderApplicationType) ; $i++) { ?> 
					<td style="width:70px; margin-top: 7px;">
						<input style="width: 13px;" type="checkbox" value=""
						<?php if($orderApplicationType[$i]->selected) echo "checked='checked'"; ?> >
						<span style="font-size: 11px;">
						<?php print_r($orderApplicationType[$i]->type); ?>
						</span>
					</td>
					<?php } ?>
					<!-- <td style="width:70px; margin-top: 7px;">
						<input style="width: 13px;" type="checkbox" value="">
						<span style="font-size: 11px;">Cyber Cafe</span>
					</td>
					<td style="width:60px; margin-top: 7px;">
						<input style="width: 13px;" type="checkbox" value="">
						<span style="font-size: 11px;">Resale</span>
					</td> -->
					<td>
						<p class="text-align-left" style="font-size: 9px;">
							(Applicable only for Postpaid Customers)
						</p>
					</td>
				</tr>
				<tr class="trRow">
					<td class="leftside">
						<p class="bottom-space text-align-left">15. Payment Information (Amount in RS)</p>
					</td>
				</tr>
				<tr class="trRow">
					<td class="leftside">
						<p style="float: left;" class="txt-size bottom-space text-align-left">Registration Charges:</p>
					</td>
					<td class="charges-box input-space">
						<?php print($paymentRegistrationCharges); ?>
					</td>
					<td>
						<p class="txt-size bottom-space text-align-right" style="float:left; width: 111px;">Active Charges:</p>
					</td>
					<td class="months-box input-space">
					<?php print($paymentActiveCharges); ?>
					</td>
					<td>
						<p class="text-align-left" style="font-size: 9px;">
							(Adjustable in first bill/period)
						</p>
					</td>
				</tr>
				<tr class="trRow">
					<td class="leftside">
						<p class="bottom-space text-align-left">Security Deposit:</p>
					</td>
					<td class="input-width security-box input-space">
						<?php print($paymentSecurityDeposit) ?>
					</td>
					<td style="width: 129px;">
						<p class="bottom-space text-align-right">Plan/Other Charges:</p>
					</td>
					<td class="otherCharges-box input-space">
					<?php print($paymentPlanCharge) ?>
					</td>
					<td style="width: 70px;">
						<p class="bottom-space text-align-right">Total:</p>
					</td>
					<td class="total-box input-space">
					<?php print($paymentTotal) ?>
					</td>
				</tr>
				<tr class="trRow">
					<td class="leftside">
						<p class="bottom-space text-align-left">Payment Mode:</p>
					</td>
					<td style="width:39px; margin-top: 7px;">
						<input style="width: 13px;" type="checkbox" value="">
						<span style="font-size: 11px;">Cash</span>
					</td>
					<td style="width: 80px;">
						<p class="bottom-space text-align-right">Receipt No,</p>
					</td>
					<td class="receipt-box input-space">
					<?php print($paymentReceiptNo) ?>
					</td>
					<td style="width: 60px;">
						<p class="bottom-space text-align-right">Date:</p>
					</td>
					<td class="date-box input-space">
						<?php print($paymentDate) ?>
					</td>
					<td style="width:90px; margin-top: 7px; margin-left: 30px;">
						<input style="width: 13px;" type="checkbox" value="">
						<span style="font-size: 11px;">Cheque/DD</span>
					</td>
					<td style="width: 39px;">
						<p class="bottom-space text-align-right">Date:</p>
					</td>
					<td class="date-box input-space">
					<?php print($paymentDate) ?>
					</td>
				</tr>
				<tr class="trRow">
					<td style="width: 27px;">
						<p class="bottom-space text-align-left">Bank:</p>
					</td>
					<td class="bank-box input-space">
						<?php print($paymentBank) ?>
					</td>
					<td style="width: 40px;">
						<p class="bottom-space text-align-right">Branch:</p>
					</td>
					<td class="date-box input-space">
					<?php print($paymentBranch) ?>
					</td>
					<td style="width: 30px;">
						<p class="bottom-space text-align-right">City</p>
					</td>
					<td class="date-box input-space">
					<?php print($paymentCity) ?>
					</td>
					<td style="width:92px; margin-top: 7px;">
						<input style="width: 13px;" type="checkbox" value="">
						<span style="font-size: 11px;">Online Payment</span>
					</td>
					<td style="width: 94px;">
						<p class="bottom-space text-align-right">Transaction ID:</p>
					</td>
					<td class="transaction-box input-space">
					<?php print($paymentTransactionID) ?>
					</td>
					<td style="width: 36px;">
						<p class="bottom-space text-align-right">Date:</p>
					</td>
					<td class="date-box input-space">
					<?php print($paymentDate) ?>
					</td>
				</tr>
				<tr style="float: left; width: 100%;">
					<td style="float: left; width: 100%; margin-top: 2%;">
						<div class="back-ground-black border-rad">
							<div style="text-align: center; color: white;">
								<p>Documents</p>
							</div>
						</div>
					</td>
				</tr>
				<tr class="trRow">
					<td class="leftside">
						<p class="bottom-space text-align-left">16. Documents Provided (ID proof and address proof are both mandatory)</p>
					</td>
				</tr>
				<tr style="float: left; width: 100%;">
					<td style="float: left; width: 100%; width: 568px;">
						<div style="font-size: 12px; margin-left: 3px; float: left;">
						<span style="margin-right: 8px; font-size: 11px; float: left; margin-top: 4px;">ID Proof:</span>
						<?php for ($i=0; $i < sizeOf($documentIdProof) ; $i++) { ?> 
							<input style="float: left;" type="checkbox" value="" 
							<?php if($documentIdProof[$i]->selected) echo "checked='checked'"; ?> >
							<span style="margin-right: 8px; font-size: 11px; float: left; margin-top: 4px;">
							<?php print_r($documentIdProof[$i]->type); ?>
							</span>
							<?php } ?>
						</div>
					</td>
				</tr>
				<tr style="float: left; width: 100%; margin-top: 10px;">
					<td style="float: left; width: 100%; width: 595px;">
						<div style="font-size: 12px; margin-left: 3px; float: left;">
							<span style="margin-right: 8px; font-size: 11px; float: left; margin-top: 4px;">Address Proof:</span>
							<?php for ($i=0; $i < sizeOf($documentAddressProof) ; $i++) { ?> 
							<input style="float: left;" type="checkbox" value=""
							<?php if($documentAddressProof[$i]->selected) echo "checked='checked'"; ?> >
							<span style="margin-right: 8px; font-size: 11px; float: left; margin-top: 4px;">
							<?php print_r($documentAddressProof[$i]->type); ?>
							</span>
							<?php } ?>
						</div>
					</td>
				</tr>
				<tr class="trRow">
					<td>
						<p class="bottom-space text-align-left">Documents should not be older than 3 months.</p>
					</td>
				</tr>
				<tr class="trRow">
					<td>
						<p class="bottom-space text-align-left">
							From 60/61 : (from of declaration to be filled by a person who does not have either permanent account number/ general index register number and who makes payment in cash in
							respect of transaction specified in clause (a) to (h) of rule 1148.
						</p>
					</td>
				</tr>
				<tr class="trRow">
					<td>
						<p style="float: left;" class="txt-size bottom-space">a) Full Name & Address of Applicant</p>
					</td>
					<td class="applicant-box input-space">
					<?php print($documentNameAndAddress) ?>
					</td>
				</tr>
				<tr class="trRow">
					<td style="width:130;">
						<p style="float: left;" class="txt-size bottom-space text-align-left">b) Amount of transaction:</p>
					</td>
					<td class="input-width amount-box input-space">
						<?php print($documentAmountOfTransaction) ?>
					</td>
					<td style="width: 172px;">
						<p style="float: left;" class="txt-size bottom-space text-align-right">c) Particular of transaction:</p>
					</td>
					<td class="input-width particular-box input-space">
					<?php print($documentParticularOfTransaction) ?>
					</td>
				</tr>
				<tr class="trRow">
					<td>
						<p style="float: left;" class="txt-size bottom-space text-align-left">
							d) Are you assessed to tax (yes/no): 
						</p>
					</td>
					<td class="yes-box input-space">
					<?php print($documentAssessedTax) ?>
					</td>
					<td style="width: 415px;">
						<p style="float: left;" class="txt-size bottom-space text-align-right">
							e) If, yes (i) Details of ward/circle/range where the last return of income was field : 
						</p>
					</td>
					<td class="field-box input-space">
					<?php print($documentLastIncomeReturn) ?>
					</td>
				</tr>
				<tr class="trRow">
					<td style="width: 435px;">
						<p style="float: left;" class="txt-size bottom-space text-align-left">
							(ii)Reasons for not having permanent account number/general index registration number
						</p>
					</td>
					<td class="reason-box input-space">
					<?php print($documentRegistrationNumber) ?>
					</td>
					<td style="width: 150px;">
						<p style="float: left;" class="txt-size bottom-space text-align-right">
							f) Details of documents being
						</p>
					</td>
				</tr>
				<tr class="trRow">
					<td style="width: 180px;">
						<p style="float: left;" class="txt-size bottom-space text-align-left">
							in support of address in countries (a) f)
						</p>
					</td>
					<td class="detail-box input-space">
					<?php print($documentCountriesAddress) ?>
					</td>
					<td style="width: 450px;">
						<p style="float: left;" class="txt-size bottom-space text-align-right">
							(Plstick ) I hereby declare that my source of income is from agriculture and i am not required to pay
						</p>
					</td>
				</tr>
				<tr class="trRow">
					<td style="width: 180px;">
						<p style="float: left;" class="txt-size bottom-space text-align-left">
							income tax on any other income. If any (applicable only for Customers with agricultural income).
						</p>
					</td>
				</tr>
				<tr class="trRow">
					<td style="width: 65px;">
						<p style="float: left;" class="txt-size bottom-space text-align-left">
							Verification: I
						</p>
					</td>
					<td class="verification-box input-space">
					<?php print($documentVerification) ?>
					</td>
					<td style="width: 450px;">
						<p style="float: left;" class="txt-size bottom-space text-align-right">
							do hereby declare that what is stated above is true to the best of my knowledge and belief
						</p>
					</td>
				</tr>
				<tr class="trRow">
					<td>
						<p style="float: left;" class="txt-size bottom-space text-align-left">Verified today, date:</p>
					</td>
					<td class="verified-date-box input-space">
					<?php print($documentDate) ?>
					</td>
					<td style="width: 55px;">
						<p style="float: left;" class="txt-size bottom-space text-align-right">place:</p>
					</td>
					<td class="date-box input-space">
					<?php print($documentPlace) ?>
					</td>
				</tr>
				<tr style="float: left; width: 100%;">
					<td style="float: left; width: 100%; margin-top: 2%;">
						<div class="back-ground-black border-rad">
							<div style="text-align: center; color: white;">
								<p>Service Details</p>
							</div>
						</div>
					</td>
				</tr>
				<tr style="float: left; width: 100%;">
					<td style="float: left; width: 100%; width: 568px;">
						<div style="font-size: 12px; margin-left: 3px; float: left;">
						<?php for ($i=0; $i < sizeOf($serviceTypeSelected) ; $i++) { ?>
							<input style="float: left;" type="checkbox" 
							<?php if($serviceTypeSelected[$i]->selected) echo "checked='checked'"; ?> >
							<span style="margin-right: 8px; font-size: 11px; float: left; margin-top: 4px;">
							<?php print_r($serviceTypeSelected[$i]->type); ?>
							</span>
						<?php } ?>
						</div>
					</td>
				</tr>
				<tr class="trRow">
					<td style="width: 863px;">
						<p class="bottom-space text-align-left">
						have read and understood the terms and conditions mentioned over leaf and unconditionally accept them as binding on me/us. If we have understood all the rates,tariffis and other
						related conditions at which services will be provided in India as applicable as on the data and as amended from limit to time. If we here by undertake to pay all the changes raised
						on account of services availed. We further declare and undertake that the above information provided by me/us is true and condition is respect.
						</p>
					</td>
				</tr>
				<tr class="trRow">
					<td>
						<p style="float: left;" class="txt-size bottom-space text-align-left">Refferal code (if any):</p>
					</td>
					<td class="code-box input-space">
					<?php print($serviceRefferalCode) ?>
					</td>
					<td style="width: 100px;">
						<p style="float: left;" class="txt-size bottom-space text-align-right">CAF sign Date: </p>
					</td>
					<td class="code-box input-space">
					<?php print($serviceCafSignDate) ?>
					</td>
					<td style="width: 75px;">
						<p style="float: left;" class="txt-size bottom-space text-align-right">SR Name:</p>
					</td>
					<td class="code-box input-space">
					<?php print($serviceSrName) ?>
					</td>
				</tr>
				<tr class="trRow">
					<td>
						<p style="float: left;" class="txt-size bottom-space text-align-left">SR Code:</p>
					</td>
					<td class="srCode-box input-space">
					<?php print($serviceSrCode) ?>
					</td>
					<td style="width: 80px;">
						<p style="float: left;" class="txt-size bottom-space text-align-right">BP Name:</p>
					</td>
					<td class="srCode-box input-space">
					<?php print($serviceBpName) ?>
					</td>
					<td style="width: 75px;">
						<p style="float: left;" class="txt-size bottom-space text-align-right">BP Code:</p>
					</td>
					<td class="srCode-box input-space">
					<?php print($serviceBpCode) ?>
					</td>
					<td style="width: 110px;">
						<p style="float: left;" class="txt-size bottom-space text-align-right">Signature of SR:</p>
					</td>
				</tr>
				<tr class="trRow">
					<td>
						<p style="float: left;" class="txt-size bottom-space text-align-left">AgentAuth­TXNID:</p>
					</td>
					<td class="srCode-box input-space">
					<?php print($serviceAgentAuthTXNID) ?>
					</td>
					<td style="width: 80px;">
						<p style="float: left;" class="txt-size bottom-space text-align-right">AgentAuth­TS:</p>
					</td>
					<td class="srCode-box input-space">
					<?php print($serviceAgentAuthTS) ?>
					</td>
					<td style="width: 75px;">
						<p style="float: left;" class="txt-size bottom-space text-align-right">Ekyc­TXNID:</p>
					</td>
					<td class="srCode-box input-space">
					<?php print($serviceEkycTXNID) ?>
					</td>
					<td style="width: 100px;">
						<p style="float: left;" class="txt-size bottom-space text-align-right"> Ekyc­TimeStamp:</p>
					</td>
					<td class="srCode-box input-space">
					<?php print($serviceEkycTimeStamp) ?>
					</td>
				</tr>
				<tr class="trRow">
					<td>
						<p style="float: left;" class="txt-size bottom-space text-align-left">CustomerAuthTXNID:</p>
					</td>
					<td class="auth-box input-space">
					<?php print($serviceCustomerAuthTXNID) ?>
					</td>
					<td style="width: 120px;">
						<p style="float: left;" class="txt-size bottom-space text-align-right">CustomerAuth­TS:</p>
					</td>
					<td class="auth-box input-space">
					<?php print($serviceCustomerAuthTS) ?>
					</td>
				</tr>
				<tr class="trRow" style="margin-top: 1px;">
					<td class="bottom-space">
						<h5><b>Terms and Conditions</b></h5>
					</td>
				</tr>
				<tr class="trRow">
					<td style="width: 863px;">
						<p class="text-align-left">
						The Terms & Conditions (referred as “Terms”) herein form an integaral part of Customer Agreement Form (CAF) which determines the use of Internet Services, Applications, 
						Content Services & Value Added Services (collectively referred as "Services") mentioned here in and signed by the Customer and the same shall be legally binding on the
						Customer. The relevant Acts, Regulations and Rules of Government of India and Telecom Regulatory Authority of India (TRAI) shall have over riding effect over this Form.
						</p>
					</td>
				</tr>
				<tr class="trRow">
					<td class="">
						<h5><b>Acceptance of Terms</b></h5>
					</td>
				</tr>
				<tr class="trRow">
					<td style="width: 863px;">
						<p class="text-align-left">
						Customer can accept the Terms by signing the Customer Agreement Form (hereinafter referred asCAF)or by clicking to accept or agree to the Terms, where this option is
						made available to Customer, on a website or web application for any service. Also, if a Customer starts using the Services, then Customer’s use of Services will be treated
						as acceptance of the Terms.

						</p>
					</td>
				</tr>
				<tr class="trRow">
					<td class="">
						<h5><b>Definitions</b></h5>
					</td>
				</tr>
				<tr>
					<td>
						<ul >
							<li style="font-size: 11px;">
							"RI NetworksPvt. Ltd." also referred as "Service Provider" or "SP", who has signeda Licence Agreement withDepartment of Telecommunications (DOT), Ministry of
							<p style="margin-left: 4px;">
								Communications and Information Technology, Government of India, for providing Internet Service in All India Service Area (hereinafter called as “Licensed Service
								Area”).	
							</p>	
							</li>
							<li style="font-size: 11px;">
								<p>
									"Customer" means any person or legal entity who has agreed to avail the Services, under these Terms & Conditions mentioned in this Customer Agreement Form (CAF).				
								</p>
							</li>
							<li style="font-size: 11px;">
							"Customer Agreement Form or CAF"means this form that has to be filled and executed by any person or legal entity for becoming a Customer, and include these Term &
							<p style="margin-left: 4px;">
								Condtions.
							</p>
							</li>
							<li style="font-size: 11px;">
								<p>
								"Customer Premise Equipment" (CPE) includes Modem/ Router/ or any other equipment installed at customer premises, whether owned by customer or not.
								</p>
							</li>
							<li style="font-size: 11px;">
								<p>
								"Customer Premises" means the address and location mentioned in the CAF by Customer for using the Service(s).
								</p>
							</li>
							<li style="font-size: 11px;">
								<p>
								"Dot" means Department of Telecommunication, Ministry of Communications and Information Technology.
								</p>
							</li>
							<li style="font-size: 11px;">
								<p>
								"Post Paid" means a method of payment where a Customer pays for the Services and usage in arrears, subsequent to consuming the services.
								</p>							
							</li>
							<li style="font-size: 11px;">
								<p>
								"Pre­Paid" means a mode of payment for Services where a customer pays a set amount in advance of actual usage.
								</p>
							</li>
							<li style="font-size: 11px;">
								<p>
								"Roaming Service" means customer access to Wi­Fi at multiple locations or area where service is offered by Service Provider or any partnerwith whom Service Provider has arrangements.
								</p>
							</li>
							<li style="font-size: 11px;">
								<p>
								"Service(s)" means all Internet access, Content services, Internet Telephony and other additional Value­Added/ Supplementary services, as opted for by Customer.
								</p>
							</li>
							<li style="font-size: 11px;">
							"Tariff" means rates, fees, charges, etc. payable by Customer for Services and related conditions at which Services maybe provided, including Deposits, Activation & Installation 
								<p style="margin-left: 4px;">
								Fees, Rentals, Fixed Charges any other related fees or Service charges, and also includes applicable taxes like Service Tax, GST. Tariff shall have the same meaning as contained 
								in Telecommunication Orders issued by TRAI.
								</p>
							
							</li>
							<li style="font-size: 11px;">
								<p>
								"TRAI" meansTelecom Regulatory Authority of India.
								</p>
							</li>
							<li style="font-size: 11px;">
							"Wi­Fi", short for Wireless Fidelity, is a technology that allows an electronic device to connect to an Internet Access Point wirelessly (without any need for wires using permitted radio waves),
								<p style="margin-left: 4px;">
									and access Internet, Internet Content, and exchange data.
								</p>
							</li>
						</ul>
					</td>
				</tr>			
				<tr class="trRow">
					<td class="">
						<h5><b>1.Provision of Services</b></h5>
					</td>
				</tr>
				<tr class="trRow">
					<td style="width: 863px;">
						<p class="text-align-left">
						1.1 SP reserves the right to verify the details/ documents provided by Customer and seek/ verify other information including financial information from independent sources as deemed necessary
						</p>
					</td>
				</tr>
				<tr class="trRow">
					<td style="width: 863px; margin-top: 5px;">
						<p class="text-align-left">
						1.2 SP reserves the right to reject any form or application, for any reason, without any liability whatsoever. In case of non acceptance/ rejections, refund, wherever applicable,
						will be made according to regulations from time to time. information provided by Customer or gathered by SP shall become the property of SP even if the form/ application is
						rejected.
						</p>
					</td>
				</tr>
				<tr class="trRow">
					<td style="width: 863px; margin-top: 5px;">
						<p class="text-align-left">
						1.3 Service shall be provisioned and provided within a reasonable time after receipt and acceptance of CAF and subject to technical feasibility. SP will connect equipment to
						the network and will use all reasonable endeavours to maintain connection and provide service throughout the enrolment period.
						</p>
					</td>
				</tr>
				<tr class="trRow">
					<td style="width: 863px; margin-top: 5px;">
						<p class="text-align-left">
						1.4 The period of subscription shall commence upon activation of the Services, post SP accepting, after due verification, the Customer Agreement Form (CAF) duly signed
						by Customer and shall run in concurrence with SP’s License Agreement with DOT and shall be subject to all applicable laws, rules, regulations, notifications, order,
						directions of the Government/ Courts/ Tribunal/ TRAI/ Indian Telegraph Act 1885, besides being subject to the terms of this CAF.
						</p>
					</td>
				</tr>
				<tr class="trRow">
					<td style="width: 863px; margin-top: 5px;">
						<p class="text-align-left">
						1.5 SP shall have the right to fix and/ or change credit limit for the usage of the services. SP shall be entitled to seek interim payments and/ or bar the availability of Services
						upon reaching the credit limit.
						</p>
					</td>
				</tr>
				<tr class="trRow">
					<td style="width: 863px; margin-top: 5px;">
						<p class="text-align-left">
						1.6 SP shall have the right to fix and/ or change fair usage limit for the usage of the services. SP shall be entitled to rate limit or bar the availability of Services upon reaching
						the defined fair usage limit.
						</p>
					</td>
				</tr>
				<tr class="trRow">
					<td style="width: 863px; margin-top: 5px;">
						<p class="text-align-left">
						1.7 Quality, functionality and/ or availability of services, may be affected and SP, without any liability, whatsoever, is entitled to refuse, limit, suspend, vary or disconnect the
							services, at any time, for reasonable cause, including, but not limited to
						</p>
					</td>
				</tr>
				<tr class="trRow bottom-space">
					<td style="width: 863px;">
						<p class="text-align-left" style="margin-left: 20px;">
							Government/ DoT/ TRAI rules, regulations, orders, directions, notifications etc, prohibiting and/ or suspending the  of such services or otherwise affecting such service
								directly, indirectly or consequentially.
						</p>
					</td>
				</tr>
				<tr class="trRow bottom-space">
					<td style="width: 863px;">
						<p class="text-align-left" style="margin-left: 20px;">
						Any violation by the Customer, of applicable rules, regulations, orders, directions, notifications, etc. issued by Government/ DoT/ TRAI, or of the terms and conditons
						contained herein.
						</p>
					</td>
				</tr>
				<tr class="trRow bottom-space">
					<td style="width: 863px;">
						<p class="text-align-left" style="margin-left: 20px;">
						Any discrepancy/ wrong particular(s) provided by Customer.
						</p>
					</td>
				</tr>
				<tr class="trRow bottom-space">
					<td style="width: 863px;">
						<p class="text-align-left" style="margin-left: 20px;">
						Upon delay/ non­payment of Tariff charges beyond the due date, SP reserves the right to disconnect the service either totally or partially without any prior notice.
						</p>
					</td>
				</tr>
				<tr class="trRow bottom-space">
					<td style="width: 863px;">
						<p class="text-align-left" style="margin-left: 20px;">
						In case Customer exceeds pre­fixed usage or credit limit in relation to Customer's usage of Service(s).
						</p>
					</td>
				</tr>
				<tr class="trRow bottom-space">
					<td style="width: 863px;">
						<p class="text-align-left" style="margin-left: 20px;">
						During technical failure, modification, upgradation or variation, relocation, repair and/ or maintenance of the system/ equipment.
						</p>
					</td>
				</tr>
				<tr class="trRow bottom-space">
					<td style="width: 863px;">
						<p class="text-align-left" style="margin-left: 20px;">
						To combat potential fraud, sabotage, wilful destruction, national security related issues etc.
						</p>
					</td>
				</tr>
				<tr class="trRow bottom-space">
					<td style="width: 863px;">
						<p class="text-align-left" style="margin-left: 20px;">
						Due to acts of God or circumstances beyond the control of SP including insurrection or civil disorder, or military operation, national or local emergency, industrial
						disputes of any kind, fire, lightening, explosion, flood, inclement weather conditions, acts of omission or commission of person or of persons or bodies for whom SP is
						not responsible.
						</p>
					</td>
				</tr>
				<tr class="trRow bottom-space">
					<td style="width: 863px;">
						<p class="text-align-left" style="margin-left: 20px;">
						Transmission limitations caused by topographical, geographical, atmospheric, hydrological, mechanical, electronic conditions or constraints.
						</p>
					</td>
				</tr>
				<tr class="trRow bottom-space">
					<td style="width: 863px;">
						<p class="text-align-left" style="margin-left: 20px;">
						Interconnection between SP and other service providers.
						</p>
					</td>
				</tr>
				<tr class="trRow bottom-space">
					<td style="width: 863px;">
						<p class="text-align-left" style="margin-left: 20px;">
						Privacy of communication is subject to Government's regulations/ rules/ directions (in force from time to time) and any such other factors including but not limited to,
						matter relating to national security and defence.
						</p>
					</td>
				</tr>
				<tr class="trRow bottom-space">
					<td style="width: 863px;">
						<p class="text-align-left" style="margin-left: 20px;">
						SP may block Internet sites, fully or party, and/ or individual customers, as identified and directed by the Statutory Authorities or Security Agencies from time to time.
						</p>
					</td>
				</tr>
				<tr class="trRow bottom-space">
					<td style="width: 863px;">
						<p class="text-align-left" style="margin-left: 20px;">
						Any other reason/ cause which is found to be reasonable by SP warranting limiting/ suspension/ disconnection.
						</p>
					</td>
				</tr>	  
				<tr class="trRow" style="margin-bottom: -7px;">
					<td class="">
						<h5><b>2.Service Tariffs and Procedures</b></h5>
					</td>
				</tr>
				<tr class="trRow">
					<td style="width: 863px; margin-top: 5px;">
						<p class="text-align-left">
						2.1 Different plans/ schemes, including the additional or supplementary serviecs shall have correspondingly tariff and terms & conditions, all of which are subject to change
						by SP from time to time, within the overall guidelines of TRAI and that Customer shall not have any claim or right in such eventuality.
						</p>
					</td>
				</tr>
				<tr class="trRow">
					<td style="width: 863px; margin-top: 5px;">
						<p class="text-align-left">
						2.2 SP is entitled to add/ alter or withdraw any additional or supplementary Services including their tariff and terms & conditions at any time, in its sole direction. Terms and
						conditons for provision of additional/ supplementary Services shall be deemed to be an integral part of this CAF.
						</p>
					</td>
				</tr>
				<tr class="trRow">
					<td style="width: 863px; margin-top: 5px;">
						<p class="text-align-left">
						2.3 The procedure for metering of usage shall be determined and/ or varied by SP from time to time.
						</p>
					</td>
				</tr>
				<tr class="trRow">
					<td style="width: 863px; margin-top: 5px;">
						<p class="text-align-left">
						2.4 All taxes, present and future, and additional taxes/ cess/ duties etc that may be levied by Government/ Local Authorities etc with respect to Services will be charged to
						the Customer's account.
						</p>
					</td>
				</tr>
				<tr class="trRow" style="margin-bottom: -7px;">
					<td class="">
						<h5><b>3.Billing and Payment</b></h5>
					</td>
				</tr>
				<tr class="trRow">
					<td style="width: 863px; margin-top: 5px;">
						<p class="text-align-left">
						3.1 The billing/ charging cycle shall run on monthly basis or such other frequency as may be decided by SP from time to time. Customer is responsible to pay his/ her dues
						to SP by the prescribed date,as applicable for the relevant Tariff Plan or as mentioned in the bills/ invoices/ statements issued by SP.
						</p>
					</td>
				</tr>
				<tr class="trRow">
					<td style="width: 863px; margin-top: 5px;">
						<p class="text-align-left">
						3.2 Bills/ Statements shall be sent electronically to the email address and/ or billing address indicated by the Customer in the CAF, or as amended. Any change in Billing
						Address shall be communicated by Customer as per prescribed procedure and SP reserves the right to verify the change of address before acknowledging the change in its
						records.
						</p>
					</td>
				</tr>
				<tr class="trRow">
					<td style="width: 863px; margin-top: 5px;">
						<p class="text-align-left">
						3.3 Payment through modes like Electronic means/ Cash/ Cheque/ DD/ Pay Order should be made at the payment center or other means as may be communicated by SP
						from time to time. Outstation Cheque not at par, Postal Order or Money Orders will not be accepted.
						</p>
					</td>
				</tr>
				<tr class="trRow">
					<td style="width: 863px; margin-top: 5px;">
						<p class="text-align-left">
						3.4 Customer owns the liability for the cheques submitted. Dishonouring of any cheque shall invite penalty on Customer as decided by SP and without prejudice to the
						statutory (civil and criminal) remedies available to SP under the law.
						</p>
					</td>
				</tr>
				<tr class="trRow">
					<td style="width: 863px; margin-top: 5px;">
						<p class="text-align-left">
						3.5 Customer shall pay all dues in full, without any deduction, set­off or withholding whatsoever, in respect of all Services availed, whether or not these exceed the assigned
						credit limit.
						</p>
					</td>
				</tr>
				<tr class="trRow">
					<td style="width: 863px; margin-top: 5px;">
						<p class="text-align-left">
						3.6 In case a Customer contends that he/she/ it has been billed wrongly, then Customer shall pay the amount outstanding against the said bill/ statement and raise a claim
						separately to prove the contention within 30 days of the disputed bill/ statement date. After necessary examination and scrutiny, if the contention of the Customer is
						established, SP shall refund (in case of Post­Paid) or credit to Customer's account (in case of Pre­Paid) the excess money collected.
						</p>
					</td>
				</tr>
				<tr class="trRow">
					<td style="width: 863px; margin-top: 5px;">
						<p class="text-align-left">
						3.7 SP shall be entitled to seek interim payments and/ or bar the availability of Services upon reaching and/ or exceeding the credit limit/ deposits.
						</p>
					</td>
				</tr>
				<tr class="trRow">
					<td style="width: 863px; margin-top: 5px;">
						<p class="text-align-left">
						3.8 Payments against the dues, if made beyond the due date, shall entail late Payment fees of Rs. 100 or 2% per month of the Amount Due, whichever is higher or as may
						be decided by SP from time to time. This, however, is without prejudice to SP’s right to suspend or disconnect the Services partially or fully due to Non­Payment. If a
						Customer is under temporary disconnection and does not apply for reconnection within a stipulated time post paying all outstanding amount, then SP reserves the right to
						permanently terminate the service. If a Customer is under Permanent Disconnection and decides to resume the service, Customer will have to pay all outstanding amounts
						and apply for fresh activation of Service(s) by signing a new CAF with applicable activation and other charges.
						</p>
					</td>
				</tr>
				<tr class="trRow">
					<td style="width: 863px; margin-top: 5px;">
						<p class="text-align-left">
						3.9 Customer shall deposit amount(s) such as Security Deposit or Advance Payment to SP as decided from time to time as part of the Tariff Plan. SP reserves the right to
						forfeit/ adjust/ apply the said security deposit amount in full or in part satisfaction of any sum due from Customer at any time. However, Customer shall continue to be liable
						for such sum due to SP. No interest shall be paid on the deposit(s). SP reserves the right to withhold the amount of deposit(s)/ advance(s) etc. at any time in its sole
						discretion. SP may call for advance/ additional security deposit for Services requested by Customer.
						</p>
					</td>
				</tr>
				<tr class="trRow" style="margin-bottom: -7px;">
					<td class="">
						<h5><b>4. Customer's Undertakings and Commitments</b></h5>
					</td>
				</tr>
				<tr class="trRow">
					<td style="width: 863px; margin-top: 5px;">
						<p class="text-align-left">
						4.1 Customer undertakes and agrees to to pay the due bills/ chargesby the due date in favour of SP as per the Tariff plan opted from time to time. It is Customer’s
						responsibility to inquire about his dues from SP at the beginning/ end of each billing/ charging cycle & settle the same even in case of non­receipt of bills or statements.
						Customer acknowledges that delay or default in payment would be a material breach of the terms and conditions governing the provision and delivery of Services.
						</p>
					</td>
				</tr>
				<tr class="trRow">
					<td style="width: 863px; margin-top: 5px;">
						<p class="text-align-left">
						4.2 Customer shall and undertakes to fully comply with all applicable laws, bye­laws, rules, regulations, order, direction, notification, etc., including without limitation, to the
						provisions of the Indian Telegraph Act 1885, TRAI Act,and any amendments or replacements made thereto, issued by the Government/ Court/ Tribunals/ TRAI and shall also
						comply with all the directions issued by SP which relate to the network, Service, equipment, or connected matter. Customer shall provide SP with all the co­operation that it
						may reasonably require from time to time in this regard.
						</p>
					</td>
				</tr>
				<tr class="trRow">
					<td style="width: 863px; margin-top: 5px;">
						<p class="text-align-left">
						4.3 Customer acknowledges that SP’s acceptance of payment from a person other than Customer will not amount to SP having transferred or subrogated any of its rights or
						obligation of Customer to such third party.
						</p>
					</td>
				</tr>
				<tr class="trRow">
					<td style="width: 863px; margin-top: 5px;">
						<p class="text-align-left">
						4.4 Customer understands and agrees that IP addresses allocated to Customer through SP are non­portable and shall continue to remain the exclusive property of SP
						</p>
					</td>
				</tr>	
				<tr class="trRow">
					<td style="width: 863px; margin-top: 5px;">
						<p class="text-align-left">
						4.5 Customer undertakes that the services provided by SP shall not be used for purposes other than the purposes declared herein and the purpose is permissible under the
						applicable statutory or regulatory provisions issued by DOT or TRAI.
						</p>
					</td>
				</tr>
				<tr class="trRow">
					<td style="width: 863px; margin-top: 5px;">
						<p class="text-align-left">
						4.6 Customer agrees to comply with the 'Prohibitory Clauses' detailed later.
						</p>
					</td>
				</tr>
				<tr class="trRow">
					<td style="width: 863px; margin-top: 5px;">
						<p class="text-align-left">
						4.7 Registration of DoT under the Other Service Provider (OSP Category): As per guidelines issue by DoT from time to time, OSPs such as Call Centres (both international &
						domestic), Network Operations Centres, Vehicle Tracking Systems, Tele­Banking, Tele­Medicine, Tele­Trading, E­Commerce, shall have to be registered with DoT for their
						respective services and location of operations (for details, refer to www.dot.gov.in). Persons intending to avail SPservices for providing the said services must first furnish a
						copy of Registration Certificate issued to them by DoT along with CAF.
						</p>
					</td>
				</tr>
				<tr class="trRow">
					<td style="width: 863px; margin-top: 5px;">
						<p class="text-align-left">
						4.8 In the event Wi­Fi network is installed in Customer’s Premises, then (a) It will be informed to SP,(b) Customer undertakes to use secured Wi­Fi network connection to
						avoid any misuse, (c) Customer undertakes that he shall be solely responsible for any use/ misuse of any Wi­Fi installation,(d) Customer is required to set up and maintain
						its own authentication mechanism for its internet usage/ Wi­Fi Services,(e) Customer undertakes to keep a log of all the events on Wi­Fi network for a period of at least one
						year and shall provide the same to the regulatory and/ or security agencies.
						</p>
					</td>
				</tr>
				<tr class="trRow">
					<td style="width: 863px; margin-top: 5px;">
						<p class="text-align-left">
						4.9 SP shall have the right to inspect the installation/ billing address of the connection and Customer shall provide all the necessary support and access to SP for the same.
						</p>
					</td>
				</tr>
				<tr class="trRow">
					<td style="width: 863px; margin-top: 5px;">
						<p class="text-align-left">
						4.10 It would be Customer’s responsibility to ensure that passwords/ CPE Informationis kept confidential. SP shall not be held liable for misuse of Customer's Services/
							Equipment under any circumstances including but not limited to misuse on account of access by third parties to such confidential password/ CPE information.
						</p>
					</td>
				</tr>
				<tr class="trRow">
					<td style="width: 863px; margin-top: 5px;">
						<p class="text-align-left">
						4.11 Customer hereby gives consent to SP to collect, use, share, retain his/ her/ its personally identifiable information, usage information, and billing information, and contact
						him/ her/ it using such information for all purposes necessary for providing & improving services and for suggesting additional services.
						</p>
					</td>
				</tr>
				<tr class="trRow">
					<td style="width: 863px; margin-top: 5px;">
						<p class="text-align-left">
						4.12 While accessing Internet and the Service(s), advertisement/ promotions may be displayed. Customer agrees that he has no objection of any kind or manner to the
						placement of such advertisement/ promotions etc. while using or availing the said Services.
						</p>
					</td>
				</tr>
				<tr class="trRow" style="margin-bottom: -7px;">
					<td class="">
						<h5><b>5.Transferability</b></h5>
					</td>
				</tr>
				<tr class="trRow">
					<td style="width: 863px; margin-top: 5px;">
						<p class="text-align-left">
						The services/ connection given under this CAF shall normally be non­transferable. However, Customer may seek prior consent from SP for seeking transfer. In case of such
						request being agreed by SP, the Primary Customer& Transferee shall fulfil all requiredformalities. The primary Customer shall be liable to fully discharge his duties till the
						date of transfer. The security deposit received from original/ primary Customer will be adjusted/ returned, after deduction (if any) as the case may be. The transfereewould
						have to give fresh security deposit toSP, as applicable.
						</p>
					</td>
				</tr>
				<tr class="trRow" style="margin-bottom: -7px;">
					<td class="">
						<h5><b>6. Shifting Of Connection</b></h5>
					</td>
				</tr>
				<tr class="trRow">
					<td style="width: 863px; margin-top: 5px;">
						<p class="text-align-left">
						Customer shall not remove or shift any equipment installed by SP at Customer's premises, where connection is installed. Shifting of connection and Services within
						Customer Premises or to another location would be subject to technical feasibility. Applicable charges would apply to Customer’srequest for shifting of connection to new
						location within the premises or to another location.
						</p>
					</td>
				</tr>
				<tr class="trRow" style="margin-bottom: -7px;">
					<td class="">
						<h5><b>7. Service Discontinuation</b></h5>
					</td>
				</tr>
				<tr class="trRow">
					<td style="width: 863px; margin-top: 5px;">
						<p class="text-align-left">
						Customer may opt to discontinue the Services by providing a written notice to SP anytime during the billing cycle after paying all billed dues. SP would stop providing the
						services within 7 days of receipt of request for discontinuation. Customer is liable to pay all outstanding amount and charges till the day of the Service disconnection. SP
						shall not be liable to the Customer or any third party for any damages, direct or indirect, consequent to such discontinuation of Service by Customer. Upon discontinuation of
						Services, Customer shall return the equipment(s) provided by SP, and allow SP to remove such equipment(s).
						</p>
					</td>
				</tr>
				<tr class="trRow" style="margin-bottom: -7px;">
					<td class="">
						<h5><b>8. Ownership of Equipment and Software</b></h5>
					</td>
				</tr>
				<tr class="trRow">
					<td style="width: 863px; margin-top: 5px;">
						<p class="text-align-left">
						All equipments and wiring supplied by SP shall always be the property ofSP. The Customer shall not claim any lien, charge or any form of encumbrance over such equipment
						at any time. Customer is responsible to protect such equipment from any theft or damage. In case of theft or damage of SP provided equipment, then Customer agrees to
						compensate with such amount as may be decided by SP.

						SP grants to Customer and Customer so accepts a non exclusive and non assignable license to use any software provided by SP to access Internet. The title to all such
						software would be at all point of time continued to vest with SP.
						</p>
					</td>
				</tr>
				<tr class="trRow" style="margin-bottom: -7px;">
					<td class="">
						<h5><b>9.Prohibitory Clauses</b></h5>
					</td>
				</tr>
				<tr class="trRow">
					<td style="width: 863px; margin-top: 5px; margin-bottom: -7px;">
						<p class="text-align-left">
						9.1 Services shall be used only by Customer or persons authorized by him/ her/ it for their own use and shall not be resold in any way whatsoever.
						</p>
					</td>
				</tr>
				<tr class="trRow">
					<td style="width: 863px; margin-top: 5px;">
						<p class="text-align-left">
						9.2 Customer shall not use the Service to send, transmit, download or in any way deal with any objectionable, obscene or pornographic messages or material, which are
						inconsistent with the established laws of the country.
						</p>
					</td>
				</tr>
				<tr class="trRow">
					<td style="width: 863px; margin-top: 5px;">
						<p class="text-align-left">
						9.3 Customer shall not use the Service for any unlawful, immoral, improper or abusive purpose, or for sending obscene, indecent, threatening, harassing, unsolicited
						messages or messages affecting/ infringing national security nor create any damage or risk to SP or its network and/ or other customers.
						</p>
					</td>
				</tr>
				<tr class="trRow">
					<td style="width: 863px; margin-top: 5px;">
						<p class="text-align-left">
						9.4 Customer shall not resort to hacking, cracking, spamming, destroying or corrupting any of the sites on the Internet, nor shall indulge in any of the offences, more
						specifically defined under the Information Technology Act, 2000.
						</p>
					</td>
				</tr>
				<tr class="trRow">
					<td style="width: 863px; margin-top: 5px;">
						<p class="text-align-left">
						9.5 Customer undertakes and agrees to take the required measures to ensure that spam malicious traffic is not generated from customer end. If at any time spam activity ­
						unwanted or malicious is observed from the customer link, SP reserves the right to lock/ suspect or terminate the link immediately without any notice.
						</p>
					</td>
				</tr>
				<tr class="trRow">
					<td style="width: 863px; margin-top: 5px;">
						<p class="text-align-left">
						9.6 Title & Intellectual Property Rights of Services & Content provided through the Services are owned by SP, its agents, suppliers or affiliates or their licensors or otherwise
						owners of such material are protected by copyright laws and treaties. The customer is prohibited from misusing, copying, re­producing, distributing, transmiting, publishing,
						publicly exhibiting, altering, adapting, customizingamny of the content protected by copyright laws. The content may include technological measures for the protection of the
						content and to use the content as per permissible usage rules. Any technological measures to alter, amend or change the usage rules embedded into the content in any
						manner shall result in civil and criminal liability, under the Information Technology Act 2000 or any other applicable law forcopyright protection.
						</p>
					</td>
				</tr>
				<tr class="trRow">
					<td style="width: 863px; margin-top: 5px;">
						<p class="text-align-left">
						9.7 Customer shall not use the Service(s) for any of the following activities:
						</p>
					</td>
				</tr>
				<tr>
					<td>
						<ul>
							<li style="font-size: 11px;"><p>Voice communication from anywhere to anywhere by means of dialling a telephone number (PSTN/ ISDN/ PLMN).</p></li>
							<li style="font-size: 11px;"><p>Originating the voice communication service from a Telephone in India, or Terminating the voice communication to Telephone within India.</p></li>
							<li style="font-size: 11px;"><p>Establishing connection to any Public Switched Network in India and/ or establishing gateway between Internet & PSTN/ ISDN/ PLMN in India.</p></li>
							<li style="font-size: 11px;"><p>Use of dial up lines with outward dialling facility from nodes.</p></li>
							<li style="font-size: 11px;"><p>Interconnectivity with other ISPs.</p></li>
						</ul>
					</td>
				</tr>
				<tr class="trRow">
					<td style="width: 863px; margin-top: 5px;">
						<p class="text-align-left">
						9.8 Customer may use encryption upto 40 bit key length in the RSA algorithms or its equivalent in other algorithms without obtaining permission. However, if encryption
						equipments higher than this limit are to be deployed, Customer shall do so only with the permission of DOT and deposit the decryption key, split into two parts, with DOT.
						</p>
					</td>
				</tr>
				<tr class="trRow" style="margin-bottom: -7px;">
					<td class="">
						<h5><b>10. Privacy and confidentiality</b></h5>
					</td>
				</tr>
				<tr class="trRow">
					<td style="width: 863px; margin-top: 5px;">
						<p class="text-align-left">
						SP reserves the right to share Customer's personally identifiable information, and usage/ billing information with the authorised Government bodies and to access information
						over the network established by Customer, when advised to do by the authorised Government bodies in pursuance of the laws of the land, and as specifically provided for
						under the the licence agrement of SP with DoT and the Information Technology Act, 2000.
						</p>
					</td>
				</tr>
				<tr class="trRow" style="margin-bottom: -7px;">
					<td class="">
						<h5><b>11. Disclaimer and limitation of liability</b></h5>
					</td>
				</tr>
				<tr class="trRow">
					<td style="width: 863px; margin-top: 5px;">
						<p class="text-align-left">
						11.1 Download/ Upload speed indicated is speed up to SP's ISP node. Broadband speed available to Customer is maximum prescribed speed for which Customer is entitled
						and SP does not hold out any assurance that the said speed shall be maintained at all times and the same may vary depending upon the network congestion, technical
						reason or any other unavoidable circumstances.
						</p>
					</td>
				</tr>
				<tr class="trRow">
					<td style="width: 863px; margin-top: 5px;">
						<p class="text-align-left">
						11.2 As installation, and provisioning of services require several vital and time consuming activities, including, inter­alia, technical feasibility check, laying down of cable/ fibre,
						proper wiring of the area/ premises, other technical requirements etc., SP does not prescribe or hold out any fixed timeline for commissioning of Services after execution of
						the CAF. SP shall endeavour to activate the services within reasonable time on best effort basis and Customer shall not be entitled to any claim or damage of whatsoever
						nature on account of delay in commissioning of Services, except for refund of the initial amount paid.
						</p>
					</td>
				</tr>
				<tr class="trRow">
					<td style="width: 863px; margin-top: 5px;">
						<p class="text-align-left">
						11.3 SP makes no explicity or implied warranties, or guarantees that the service will be uninterrupted, error free or regarding the Service uptime or quality.
						</p>
					</td>
				</tr>
				<tr class="trRow">
					<td style="width: 863px; margin-top: 5px;">
						<p class="text-align-left">
						11.4 SP offers no warranty that any information, software or other material accessible on the Service is free of viruses, worms, trojan horses or other harmful components.
						</p>
					</td>
				</tr>
				<tr class="trRow">
					<td style="width: 863px; margin-top: 5px;">
						<p class="text-align-left">
						11.5 Though every effort is being made to provide best quality of services, SP shall in no event be responsible to Customer or to any third party, for deficiency in data
						transmission or for any inconvenience, damage or loss that may be caused to any one or of any kind arising thereof.
						</p>
					</td>
				</tr>
				<tr class="trRow">
					<td style="width: 863px; margin-top: 5px;">
						<p class="text-align-left">
						11.6 SPwill not be responsible for any liability on account of any of the content that may be communicated, disseminated, transmitted, downloaded, stored, either on a
						permanent or temporary basis, or in any way dealt with by Customer using the Services.
						</p>
					</td>
				</tr>
				<tr class="trRow">
					<td style="width: 863px; margin-top: 5px;">
						<p class="text-align-left">
						11.7 Under no circumstances shall SP be liable for any direct, indirect, incidental, special, punitive or consequential damages that may result in any way from Customer's
						use of or inability to use the Services or access the Internet or any part thereof, or Customer's reliance on or use of information, service or merchandise provided on or
						through the Service, or that may result from mistakes, omissions, interruptions, deletion of files, errors, defects, delays in operation or transmission or any failure of
						performance thereof.
						</p>
					</td>
				</tr>
				<tr class="trRow">
					<td style="width: 863px; margin-top: 5px;">
						<p class="text-align-left">
						11.8 SP shall not be liable to Customer and Customer hereby waives and agrees to continue to waive all claims of anylosses, opportunity loss, fees, expenses, etc. direct,
						incidental or consequential arising out of any delays, failure or deficiency with respect to Services, as a result of reasons covered in clause 1.7 hereof.
						</p>
					</td>
				</tr>
				<tr class="trRow">
					<td style="width: 863px; margin-top: 5px;">
						<p class="text-align-left">
						11.9 SP will not be responsible for any acts of franchisees, trade partners of any other third parties with regards to any scheme/ plans, which are purported to have been
						offered on behalf of SP, without explicit authorization in writing from SP.SP shall not be liable for any acts of commission or omission of franchisees, trade partners, or any
						other third parties offering any privilege or benefit to Customer.
						</p>
					</td>
				</tr>
				<tr class="trRow">
					<td style="width: 863px; margin-top: 5px;">
						<p class="text-align-left">
						11.10SP shall not be liable for misuse of Customer's user account or equipment under any circumstances including but not limited to misuse on account of access by third
						parties to confidential password/ CPE information.
						</p>
					</td>
				</tr>
				<tr class="trRow">
					<td style="width: 863px; margin-top: 5px;">
						<p class="text-align-left">
						11.11SP shall not be liable for any claim, loss or damage of whatsoever nature that may arise due to installation or use of equipment, including any installed software, in
						Customer Premises, that are not provided by SP. SP will have no role, whatsoever, in such equipment's functioning and warranty, etc. and will not be responsible for any
						defect/ fault etc. at any time.
						</p>
					</td>
				</tr>
				<tr class="trRow" style="margin-bottom: -7px;">
					<td class="">
						<h5><b>12. Amending The Form</b></h5>
					</td>
				</tr>
				<tr class="trRow">
					<td style="width: 863px; margin-top: 5px;">
						<p class="text-align-left">
						12.1 SP shall have the option to add, delete, or amend any terms and conditions forming part of this CAF due to administrative and commercial compulsions or for any other
						reason considered necessary in the interest of business operations. The Terms & Conditions would be updated on the website. Customer's continued use of services or
						payments to SPeven after such changes will constitute Customer's acceptance of all such amendments.
						</p>
					</td>
				</tr>
				<tr class="trRow">
					<td style="width: 863px; margin-top: 5px;">
						<p class="text-align-left">
						12.2 If any part of this Form is held invalid, the remaining provisions will remain unaffected and enforceable, except to extent that SP’s rights/ obligations under the CAF are
						materially impaired.
						</p>
					</td>
				</tr>
				<tr class="trRow" style="margin-bottom: -7px;">
					<td class="">
						<h5><b>13. Other Matters</b></h5>
					</td>
				</tr>
				<tr class="trRow">
					<td style="width: 863px; margin-top: 5px;">
						<p class="text-align-left">
						13.1 Any notification by Customer or SPto the other party shall be given in writing through electronic mail or through post.
						</p>
					</td>
				</tr>
				<tr class="trRow">
					<td style="width: 863px; margin-top: 5px;">
						<p class="text-align-left">
							13.2 Where two or more persons constitute the Customer, their liability under the Form shall be joint & several.
						</p>
					</td>
				</tr>
				<tr class="trRow">
					<td style="width: 863px; margin-top: 5px;">
						<p class="text-align-left">
						13.3 SP, without notice, may assign allor parts of its obligations, rights, or duties under this CAF to any third party.
						</p>
					</td>
				</tr>
				<tr class="trRow">
					<td style="width: 863px; margin-top: 5px;">
						<p class="text-align-left">
						13.4 The CAF binds the Customer and his heirs, executors, administrators, successors, and permitted assigns, wherever applicable and also binds SP& its successors and
						assigns.
						</p>
					</td>
				</tr>
				<tr class="trRow">
					<td style="width: 863px; margin-top: 5px;">
						<p class="text-align-left">
						13.5 The information provided overleaf shall be treated as part and parcel of this CAF.
						</p>
					</td>
				</tr>
				<tr class="trRow">
					<td style="width: 863px; margin-top: 5px;">
						<p class="text-align-left">
						13.6 The headings are for convenience sake only and shall not affect the meaning of the provision thereof nor can the provisions be interpreted in the light of such headings.
						</p>
					</td>
				</tr>
				<tr class="trRow">
					<td style="width: 863px; margin-top: 5px;">
						<p class="text-align-left">
						13.7 The failure of any party thereto at anytime to require performance by the other party of any obligation/ provision of this CAF shall not affect the right of such party to
						require performance of that obligation/ provision subsequently. Waiver by any party of breach of any provision/ obligation of this Form shall not be constructed as waiver of
						any continuing or succeeding breach of such provision, or waiver of the provision, itself or a waiver of any right(s) here under.
						</p>
					</td>
				</tr>
				<tr class="trRow" style="margin-bottom: -7px;">
					<td class="">
						<h5><b>14. Dispute Resolution</b></h5>
					</td>
				</tr>
				<tr class="trRow">
					<td style="width: 863px; margin-top: 5px;">
						<p class="text-align-left">
						Any disputes arising between Customer &SP will be referred to a sole Arbitrator to be appointed by SP. The provisions of Indian Arbitration and Conciliation Act, 1996 or
						amendments thereto shall apply. The Courts in India will have jurisdiction for the purpose of this CAF.
						</p>
					</td>
				</tr>
				<tr class="trRow" style="margin-bottom: -7px;">
					<td class="">
						<h5><b>Acceptance By Customer</b></h5>
					</td>
				</tr>
				<tr class="trRow" style="margin-bottom: -7px;">
					<td style="width: 863px; margin-top: 5px;">
						<p class="text-align-left">
						Customer has fully read/ has been explained in vernacular, verbatim the terms and conditions of this CAF and confirms that he/ she/ it is fully conversant with the Service(s)
						provided/ to be provided by SP together with its tariff, specifications, requirements, limitations, etc and has signed this Form knowing and having such understanding.
						Customer further confirms that he/ she/ it has understood the contents thereof and has signed in token of unconditional acceptance of the aforesaid terms and conditions,
						with the understanding that this is a valid and binding document and can be enforced in accordance with law.
						</p>
					</td>
				</tr>
			</tbody>
		</table>
	</body>
</html>			